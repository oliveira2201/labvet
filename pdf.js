gerarPDF(dados) {
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    let hemograma = {
      pageSize: 'A4',
      background: function(currentPage, pageSize) {
        return {
          image:
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaAAAAGgCAIAAABjVCQOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAEc1SURBVHhe7V0Ld6o6Ez3//0d+51xb21prWxV88ei3M4lKISBiAnnMXnvddW5rhYRkM5OZTP78MBiDkZflPhfcZfnn4cr3XfaSDOFbKr+hWB/VNx8LdS0G436wwDE6UZCEpaRfH3spQ6f/Nqf/rcfmv428Om4DN4Nbwo3h9tR9Mhg6sMAxzsgKSMZVyJ63dYlxlbjVq/DtMjREtYgRPVjgYkX5AzkrNicpZ6e/ddXwm3/XQvKWO+nqorGMOMECFw1I0WDjZK9pNvPGOjNFNBkNFyYe611MYIELGvA6k0zYaPOkNuEjJ7xadAs6h/3ZsMECFxrKUwG/LFuMYaYJs0hGPxeN6Ocwbk/ye9Q6IGhfmkUrFqm48xOLXWhggQsCcD/TLF/ubIiaWsJf0RI+ydBUsUt5dSgR7kQ42haWDtGB6EZ0JsdnwwALnMdQxtprWpulgwlzSdpi8N0mFLJ7IYVP2H2QeKheo13DiN4ovo+ciOc1WOD8g5jMJow1YZpRYq3IKQtrGkP6Ze6emYjKf5v8ncw6hm9ggfMGUtceSbJVi02bU3RWicyJ+T5C0B/KUv7HSucZWOBcB8Qo/9gPnpbZEy0qcbiwiseDy6x0noAFzlHAyRIzcJB7JUTtYy+mH6+U94Ay7oYtZUql27HSOQoWOMcAZ2p7GmJZ/NtI95MtteEoSmHZvQ9ZB8CrCCr5k/MbxS2wwLkC4Youd9Cp2szppkhYXe3LQ66+hWEI4nGs9ujeWoffJF4zbNC5Axa4qVGUMLvgVNbmSTezl0QYa2wvjICcHtCd2Sds0DkCFrjJIGwEeEP3pKqyrk2JYUrHBt2kYIGbAGWa3TVPWNfcwv1KB1e33J7UnzNGBAvciCh/xMToHRiF38q65jSk0vWPCP23KdZHDm2PCRa4UZCX+eehb2zu71pkHnDcwB+InJ7+AaJ/m3y15/fWOGCBs4zsjoU2ZbLxG95T3Jnig4HBOT22wQJnDVLaGsNaQzbZwoIw6FZ9N5+wzFkFC5wFwCH92NfGsZYimYAXZcKFMOj6JQCxzFkCC5xRFCRtPRxSDqvFg3LXN2jOMmccLHCGAGn7PPRZZs7e0nLP3mh0UGmPjfHQJMucQbDAGQDczNvSJhfauCh25MioNkyPFyE+xmsXj4MF7iEI7+PmIgukDYOV0wIYF/S09/9R3hzjAbDADQRssT4FdoS7wdLG0KKfzOENypu9BoMF7n7ISEJjINbIKymMXugXmMreUh5OA8ACdx/6LLfxWGTcDZla1C1zf9diCwQvzN0DFri+KPf5zeU2OK0lH8LkFU7FKTklq8PqLX17Tp6ftk+z7Qz/BefJHD9c7pffx+9dtssK+35inwzK/zacYNQfLHA9ACfiVoD/kYWSrMwO+SHJkvVp/Xn8XB1XmG/4x/fpe5tt9/keH1AfZZgAHUGzRw9Dv4SiJTPBbQvPv4X8LXYLPJRDcVBfZAd9lnfxAV7b7QMWuBvA2/LGnptBoa6iLNQc21XmWBu3s5f05ePwARFksXsEx+KIPkdnXjpWcFPhRddqP5e/Oj8RPDW8e2Bvqe+1AFFTq7vwzN+1qKnJ6AQLXDvgk9x6kYr8jzvXROATYY7N03nrHGtSfoY+/5Q8ve/fbc+uwEAH/6eL/eLa583uvcnanyQzPER4rxYfRPkDCete883mCa+KdIAFTg8xsDpXfMVy251Zu3BtYIUNmWO1T9I3wGPCt9l2l3xHKY6RSV7TV9Xt1W58hJVnAZlbn9bQUHVJ48jLbHHrRbva27u+12CBq6M85DeOGvlvc+9yG6y2q7Q9OMeqf05fCMMkzVN1JUYFu3wHX1L00qXfLt1ohJUHgQsdcosvGzEsO2NcYhWYC9I0wAL3CzcNtwFxerze4Vdepa06Qx6h/DaQZA4TDF6YumT0OBbH5X45SysdVe06s7x8fzL7On7BZlQ3YQG9xiebchWwwJ0BR6BzxU3U/7jzDQnDDebVUyoyDyzOscsES2fv+3erdoQXWB/tvFG6Ka+Vzha7hdWEEhFj7axNIlbleMvzGSxwAuU+7wqVDgpXwZ56Tp/Hm2byKmTNrY6rOEMQ4o2yo0jCOH3eJD2CeTq3/ZoRwf2O4MPfNefKSbDA/Yjiq7XxUaFIOLp/WwLc0qt/VJ0AtikndiLSSnbZTt1NHBBvlGTEN0ob5aWTmfUVg1vBBzF0o9/2ELfAwZfoqKD/b2DK+OqwEuo24TST101nn8dPi9E9l/B1/JrmjaLluf+3p626P2sod1mX83F/QCwwxCtwZZJ1GPkQvmGZ4h+7D7Ho5sg0o+DDsQg5HbQoCxVPQHtd6HbJETUOXZC9dZlyIvIQKyIVuO4df/nnwAWUj73IBZH7GZ0gadxz+rzLw3RX8zKXiSAO9fmFZ40bJ7otVuXaA6yDX9i+Iz6By8sutxQm/dBkoqtnWh3lk/M8zdantbrRUCB2muwog9e1Pr9Q3lgy2+dj2FAiwGpnbPuLuAQOD7hjwSJbDF+U/T5+Q0RctCNATDOQluTU7foPqJvaUuqsuknS7T0nz1ZzR64oO4Nmf9fi4N2YEJHAiVJuted94WNhdfggwnY7j2YXeda41X6lbtpnZKUn6iaJm6TF0NECPt1pT+JFHo23GofAFV0BdbHH5YHEyGNxFGmlchxXh7Vr3JCBmc4+dh/q1v1EURZqb6kX6iZJGvd5GNGCxphvT1zPnrcDkp98RPgCJxYm2jfxPeKWAngn+zXZZGmmj72vGocOv6byNlrnNHHDYwUcLhBnPjTGvOK/TQzHVwYucOWx6MgFefzIIryTvZts0o5bHbz0VVXNAu/U7cx5Oh95k4lIlGubAhFseAhZ4MSjbQucm3h97fO9WHrzcLJJjfs6fqmWeAKnIzl9iKEyifmcFR0FcsI+mTBYgetICzKSEwRfSaxz10awLzyvx42RhmoI6nVCN/+rLX4RN5+OlDXyC+VPvmwtux9wJnCYAtcRMMVjNhJCEtaEz76SuvNk5kX1EWGCJM+/7t9TUre/pq9Wqyq1odi0vvXFKZchIkCB68gDMpUElJWZH5HTbtLNz9O5++c8LFI/AwtaohXJLMkS1bZx0bEqLTbnB5c+EprAtR5/9XddJsamsdySFcJ8o8m22C1Uw5zE+rgOR93OnCfzopwoUQP2cMtxNiJ9JKwCJAEJXPnTuuX4n8maCsfiKOYbhmkwBkU6+zo4GnAIrbcl6b2yOW1UI8dHXraFHbKnoFLkQhG4omwtcwp1M3rskKhdEZJBIRvi6mLcW0qHKoSkbiA1B0bclMWs2qcM7LtgTuoKQuA6Xkczw6+jU3ESq2+BzTfiS/oymdPUgs1xE6C6SaJRIxTF7AacnrbdDjALgtiZ77/AdS8omC4RIworBjnlaL45lf0bSCSnjdThsE9Va6dD17K1/8Uy/Ra4rpDQS2J8uRQGjkpWCHfKueOofuxCieS0kZo2QU5cAx2JB75vdfBY4Dr2oGRvVgLeSZaEPOWoXSJFy4FkAehs4OoGonXOWM0dqaMDTlxyB74KnCg4PnrK4vv+PXybIpmtj9OXxgwq8a2TU+aL/EbH5p/8w9etDl4KnFC3xjOQHFxt/CYCXxKSpKbBDZ/21MFdtrvcTOCkN4o7BeU7tm+LLUAewj+B63gGVquVbrNtFDYFTblp62KK1BB5J9UbC5LU2x8Hh6pXiarXLSs/9qwHe/BM4ERUQatuRjcqaBFa+lsbZQOT2amYZnVZmG9xOKeC1MyJE+IawCxry0zwLubglcBlLTFToxsVtAg8floj2jhdUUx5SlYsAgdSSw+FY8ZR1lom1q/cEX8Eri3fzfRGBS1ErZ7YZl0yG/9A1SiCpzVSV7t45lnRkj8Pb8mfUsCeCFxHX4/yPsH4i1DgxjfiYlkHqJK6Gg1XXeAUJrUqjMAHgSt/2jbNlelI1rLvlbLvJlq6mT0lT2OuxF23wUUlcMSX9MWpZbgrWtaFjG+CtAQPBK6tRsiYJzyKk2UwEKOaeGjsuAdBfR79O+DCAM/tnSqqcxNtkT1Rd8T52kquC1zbRrkxI9Z5matzAGITuBFz4oqymCfzy3XjIprsUjZcE225WWK7t9sS57TAtR16NnLO4aE4qFFYHZQxkCbeOGXLxOHZEZpvktTPLsYZKhD7HBozEXS8DrC7Aifqxzd6ExQnmY6LwLegdpCaPM7u1BjDCxeSwMFDV33hKopv/X5Vl89zcFTgylS/GWuS18X3yfPzZR4hzT3bFS+i2AbXQepkRwOpv5F/6OuOOLtZ1UWBc83hXx1XkQuc7XwReMHx9jBInfy2m742XB+0LYu7WXTEOYETIZu2sPREIZvwi4h0kFptO9Sw2MVSO6SD7maK1NBeB9jBjVyOCVxWnP5zLunmdRdfjkiVZF/YO+Yudv8UpIY/bZ+mLeJyB6Bxkybe94dLAode025/mzRtuix9PsHeCEngYMaqHjGNWMq0dJOa72wqnAZF2brJ4eRQArBDAqdP6MU7YdLDL+LaZq+ltC8SW/ZF1PHTC9F8+8Ecw2jzt54cSo5zReD0EWgHLF7hQEWY5VsjTT8bR0Dx+0OReniq4+4Ho3XF/G3S08IqcELgyn1e6yBJF9Ys4TXUx2KEpOlnoy5jdGVa2kg97HiurxZi8upyHoq1E0FVBwSuZTevI5k1LHCCJEA2Tg8I9hjGe0kC536urxZtOfkuVFWaWuBawjHi0D833Ph492nVSDPQ+CIRJ4goUveujg6dS3sX8qUuOe6/zeQVRyYWOH3SIPrF9IHNgxFjCUYtaQbC4FL9YgK8AHclda9ThzPcB1gqc01NM/xwWktlSoHTH8U4ddi0BiFwcvxVh2OEpBkIg0v1iwlw315J3bs8eLBbqxV5qQ2qTnsc12QC11ZkypG1yQt4EipSD5jd0hBdneQOksDZSzYcB20BB9sHQnVgIoFryel1sCwBC9yVNAkNLsNFVye5g0EIHKB3y/5Nthg3jcBpaxK4WTyPgwxX0iT8Pn2rrnkYMdZJbiP17WJvcgVgKmgX1kXYcApMIHCiWEij/RNqfDeOxVGNv+pwjJM0CU1ZGXB1OYP6yoAErtU/m+Lc6NEFrmUl0tkDZTkP7kpSIlOnFLNp/ItGXx6To8S0aS7GTRE/HFvgtIVWXK4ImpWZmoQ8Fc/9YGRPeLx1krUMS+AA7WKcKAs07jLUqAKnzXiesNBbH8CTet5yrtaZNA+NxBl4D8MvUsf6nSbSgPa0z5GtmfEETm+1wjl1KeutCbhj8Z721CTNQyNbJjmE+ovUsR4n+mrRth41YtbIeAKnTXSeZN3xXnCw70pz85A3af0iday/W7XaoD9cZcStSiMJnN4hn/tRHGax56l4JnWCkdMDVBlR7lVJEjizO+EcgTZrZDRHdRSBg6XadE5dzQtpgp2pK6kTHg+kil2ovLJZJQncOEfQjo3yR1v7d5xSj2MInDZy6mxeSBO8HH4ldcLT9ikrHxqdnHxTJwmcjZKiLqA8aAo+jhNRtS5wZaJxwqdKax6G2A+1q5H64Vg8tGWYk+DqJIHzrGT5PdDWU8pX1ttrWeAKXRjl79oX51SCq87+Ik3FXf7QGgr+nLu0SSMJho6i/NFHVC2fJ2VX4LTri24eENsB4U/xVLyQBG572qreGQQ+SesXqR+et8/GCyY7Be0eTbED3SYsCpz2pAXb7bEBrsv4iyRw38eHttzjz1ngrqR+eElfVO+Ei2yhWY63avFYE7i20Ml0J5w+grfdG09IRRK4B08PWB1X3J9XUpeGtE+rFdrU379re2lxtgROm/g2wpqiJXCmyJU0Gx/M9WWB+0XqUk9PnLkX5Va3X3NhK3xsR+B0iW/j77M1CA6kXkmz8cFdk/zC+EXq0m3m3+rNMOjzxuz4dlYEThsSnvwI50fAdX2vpNn4oD/FAveL1A8YY6p3gkem2ZZuaXXevMCJwxZ+3zrozknXwyBqinCcQZIE7sHSjDAAWeAUqRPMHnbhPvKVpqa3jU345gVOUyPl77o8eR//5s3hitQDb/uHtqMu9yxwZ8r+NLG91ydo0+L+2xhfxTIscNriAY6cUf8gPo+fPCcFqQded6+qXwaBXdQr0Qkh1hG5CW20wXh5IaMCp00NgSo7XM+yPzj5XpEFzixJ4JLMp82LppA9N+TCdMqISYHTpoYUm0B2n/AhKYpS4NKHBI7N4SupE0LepNUO/Xq90ZQRcwKnTQ3xcN9CB3gZTtCEwHGBFkXqgZf0pSxD8HIGQLu3wWDKiDGB0x51Wu6DCgx9n3iDkWr+g4vifKa9Inogma0O0S3AXaE1jMwlXRgSOMt36QhUFTMWuIfTRPhILUXqzFDLwPVE/nmoSQdoyogzI3B6883PbacdKH9KLrQt5+SDOxlE4jQLHDX/afsUVQacBrqUEVOrWyYELg7zTYJXx6XAPbgXlSv6ClJPRrHH/ha08ckyNZD3a0DgNObb3wDNNwkuRSun5YM7wzkkLUg9GeY5DPfCmhH3sMDpzLcwMnu1YC/VyLTkbhSktseZINKEJSPuUYHTmm9QPfXrEBF7sUYSuMfXxWPfrUXduNg9FKsJCnaMuMcELjLzTSL2WCrNzMdLX8SeCkfdGE+JpD6wYcQ9JHARmm8S7/v3SCfnuckPHhsIwAaMV+Co1Rw/raP8yZ7qm7ceNOIeELgozTeJqCenzLzHYHwMURvCaPXDkeggoa3W8YgRN1zgiu+GPRmH+QZges+TuRqp1YEbPGlmGslsiPooH2pyRBUu7wFMtpqwZK/DF3yHCpxuRTAS800i0s1GJHCmTg+I1NOn9j64mTdgFBtNGaXBaWcDBU6/HBho7psWIpMriS+TiwTOVG2fSPf2Uh8+eLBsyNAaT+8DDxofKHAaMzLQrQsdWB3iOxqKGmsqdSvaky7myTzsM54fhMHlryECp18IPEQXD8rKLC4jjpppJMIggUke3VImWvrwsdnhQxvAHHTo6BCBy+b1UxeMbKrwEat9TEYcTU6zsb+40n2pmbGdLzMMmhS0QSc23C1wpa4I5+M7KjxFXEYcCZzZ3NS4Em6oA9l864VMozPF+qh+2xt3C1yzAqc40TlifB6iqS+ymUHNze6djChWQw1k860/sreG1NzvKd4pcDrfeICshgSM1yjyucj6sLF3MpYzUqkD18e1ajbjFvTO4p1r/fcJnCY75J/5owy9w+a0CX+Kyvl5Mj8/4zmuzGB8JhI0szXuzRe5T+CaO8WiSu5tQ1mWr+lrbTSHRhKgY2HeWo9iWwialsZemnwANGen/l3fdQzpHQJX7vP6xSJL7u1A4DW40a5k9pbaOn098Moi1HtG9rdZAt4xjpqW5Q98xJrm3LUmdofAacILsWaHaBFy3i9NUXvJ9yoYjauE13uyUYkrhS0hZLiTXb7bnDar42p5WL7t3uA7g/gH/vfz+LnNtofi4Egqcr7cPSI7vQVOG14I5VBnI7imrQZGEh3b4b9gE+JI3aZNDZGitsk26GQImbgrdHWVl7ut/ASDGXqX5um0Yd8HQw19BU4TXoAzzAumv7HP97M0uFlKU9T22Z3H4ijmVYhd95q+TuIA4qIwxGCRqQXis3KpG7vJ85/M0zmevo3l1554JNTQV+A04YXlwO2vYSPAtDi0ZRQPKzQj7qwR40sDXP71aX3VNXkzkpcbu8nqn5DSfRw+JpE5TX2R3qGGXgKnDy/Et/m0D/DaFAMrmImKViQzSI9qnk2IE8sCE7h07EOz0IeQIbGg2dS1wbx8CckcTMKxl+fKn8HrY70E7sF1vtgAYyec7Hwa1pg2qm2W8bH/CETjqN/GeTFIHPKDMoGr0la9pccpvzMRCX37fNT8MPikdQl66VWzq5fANcszRb574SbEFsu0MT68I43mMfMbAtnbSzcPFRhneR5uI6w2JW1Sg6z2nvzyZPZ1/FJ3YB/wF2sSBP70KKB0W+C0/mmfr34csIRhDUEs1qf16iBC2iDmG4h/4KGii0VIOz9gYqi/cQbidMFUHCzya3D4RQzl0ZeQvD+Vke4cMj2C2YsJgimgHFJcd8xOw7VSYaKO5q5ms3oYoPi+PTJvC5zGP+1nHA6DDP18n76hYmqPp3w1dRLP+G339nn8hOU89gJBO/xeNcdtJzP4jKoxY0EtYsobqN6PF5T3PMqmhV22EzkfcoBN0Vfi5Z3MMO/GMVTzVb2AUja/LUS3BU7jn9pJf4OxhtdRPaQtH95Nyk6nv5qncyjdaMtGHcBcXewW4q4ud+gL6YbxgsmKCUxjtS3kfBvecENzPrW+ox6vcJVVThedspdw6WT2unsdYZzovdTshjVzQ+DG8U/F+uhhqSxt2WsXVnuzg7U/IaWDDTjyUmgTGIteBlVxt5OeG+CjoyrVDS9X1QY7wGSBoDjUOTRUcEsjeE4DvNQbAmfbPz0WR0ibeFrygUnWevBeXr6HvhYyhzGhrjcF8HJza0TeJHXd+27ivZN+Gb9S3cyWO24CrxwXgzA0YPC84LKoG7UDjZd6K53jhsDZ80+h92qLtUFpq1F+J11iuV9O6LR6p3HCOZ06boMbmKd+VBmR6mZ7s4fIIZehefc6RK7H2V6xFTvOfssRiB+qX+vQJXD2/FOYVOOtj8pLkMytjqup5q0fGifvLZ3tMie2qfixGId7s6xusAbgiAh1w7Vc7Qqp8rYTmzVbqj67DJcugRucXNcNdWQyOmXMRyVHRjJ7Tp832ajJ5RdgmEq3SwyF2u25QTlGnTo0IMkSMbFxew5ObDmoLK+7eWP+0+3Bg7a6qw9yVhelTi+1S+A0S3oP+6ci+jPhi0heNBULc5Psqit/SpGT6eSreBwvYwBEwWQHe+w8lmxUOb4AYnH1dapXd5O4ScvFBbReakcstVXg7v2im0CbRV7Y5CNVXp1S50beJ3iBzAFWN1O9t+ko1e199257nXgYhNXvlMbRbWAIWc13wxQUq5C+qJskbtWyE9A0vMptq+HVKnDN+kiP7D8tS1I3d7wzOWIoFXuSVTlMDJUW48DYleoG93mESP9gKDsONzxtj+HqIO3HtBq2Uuomr1i9AcdJ/WPVUW0emZq9tb5mWgUue6mf7jzsZGkJsYnatX1L55GKYTRJuhx8ZLEkN7Vhomy3/bvL6iYh1uPGX72tUl6X0kGsdldWZMIzvVzRL+KebRYaKHdZTZo6jr5qEThdfZJyP3BDhvTIxERy8GnJW0rGLmsjAcNW5crIOxm/f3BFmq5ueqZNHPLD1Wsbs7vk5ShIZTv/OS9z7ytuUV/ZsnDvUSe9wJWpTiMHQRW5lW2udoFrTEUSCRRH3feIwKR9271dZa56V/YoL+ThQetZmamciUsrrBKXkFehBQ17npcE3jR+ZTjriZsnt0C1yjT6+5d6gWtuYLj3OEIJvIv8WEeQg3g6Tw3Den1aX1flbHcXNRaPxpF8twEYo7su30wrbnCQ1bVtIrCKeJZyFfpHCPQCp4lTJENW4j17WvRIXkfZOawFbBNYkaLHLvPWbNddvpDKT097mMjjwOR5P1RMOYN9VekoyOjX8Wuc154MFru1VD2Y6EAaZqptRtE/x0MjcJo/vvOwVQmVhm5w2I1Aeip4Xdv2RDpwKETpAdF1pmSu+iWUprTLwzlPA0aocvCNdFeloyBtn8fP0YLsYjEHTZD3cLkff0k9iT601IE9k0U0Aqcx/wZtYHhLadh597Rww+S+TahxAMwTvP2UFybvSrJ6q928/AlI8x/G6Tbb+hJP6A+0KM3SxZ6WrgZ01+XD8vPniltjDgA4Dar6YUhEfya2EqE1+6wWmmQRjcBlb/UDnvtUzqxBvY7kiPGOdNsY5VP5qhdgjn0fv1Uue23qNln7Lf73/Ffv+/c0TycJoYwJjDq8FYRS9OmuC/Gxc1/BGNycNuM77++7d3EDl5sJg9QceAyqkUbRDITCplO/q0AncE3b7/4DtLyPBOHOyVedXOMAWChwKn9NXbB2wxeeP4APw6jB+3NaU3R8QJ6g5ugulUp26THwdxdJPm2fMAknLJK6PtLu7MDUTZIadcwthBqKsiZTYLMUSEPgMt0C3J0vfsyoEB4YaRyGvjsZsLgTKN3X6Wu5X+LG5sn8efuM+QniH/hfGCDLw/L79I2P+R5DeBx4McDTT7IE4oVugdy/7d/gp+O/+N/VYQX1R0dNspXlAtyhmCxyvFWHXxikSfRl53iaZmURmHXqd2fUBa7c1s9Y7VP4vAaVvBrAA6PHA2vUTecOegcVw/zEf8HwFteCBx6Zr0vVPUntsuSlNpfh8o96Nlxd4Jr7vJp/0w08M+UahPHM0IrUxRobjAAQsnN6IbXOhpmsscYa4dC6wGXPt62+bhyKw6VVYVDkJVmuisOIEJjzIkqOMRa8wCUzG2nS+oS2327Mb4HTrtvdWSLp++T5uZZNyrY4U+eWEQZkfZ3A1Q0kgbNU8fj0r36mQnn8pVe/BK5Zo1wbee1GCDvpWvicTnOMHiM8qD3awasbSG18S99Uy41Ck9O2/hWx/SVwmnLA7YWWtMjLPFirm15EeE68ls94EBhCr/L83xhIUvC8fbYR1tfsSvid7vtL4LLXG3J4E15uz+pPtCudfR2sxLwZ8WB72kbhnF5ILYU4qPabw02n85fANQ8JvDfFV+wWDv7JJTP4F6rBDMaduKYZxEMIQjLbHC2UXNTVhqtunK8InDbCcOce+0jWTefJ3J3sX4ZfELXXozLfQBK4z4OVs8c0iR8Vs+wqcBpj7+nuCINaWQj74dHTsn3KLyNIwHzD21GNouqgCps0ZSwVMc8WXQtrV4HTLNfdGWEQFRG2VBEhhofHjirjfkS3+iZJ7bUUSC2+68KVL6/pXFeB01Tx7TwyugkRYTg3JnBSG2GuckSV0R/X1bcoBQ6mq+oIo9CUFalU970KXDavlzm/dw9DXG8nNHOio2oYnmKX7WI030BqMtw71RFmkTeCB3+vm46uAqfJCT7duYfhGNwehg7KZ5ZYye5hBAlxVk6cAke0JXCd2qUETrOr6/5jtMI5L6Mn0dJkttpztIFxG6osUqzqBj5tn1RfmIbG+zyfIXMWuKYfe3+VJFGVFC2JSuDw2Gwe4s0IBp/Hz8gFDm1XfWEamvjB+RRBJXDdkYieeE0iyBGpEY21eYg3IwwUZRFjdshv2rPgik2jbtI5A0QJXLN0HP5G/qo/XpJYI0TWzn9khIFdHmt4QZIabm8NriOHVwlc86Tocnd32YyIkuCqJIHjipiMDqhzIOMWOBhAqjuMQ3vQAuEscM2DZu4MoQKiMGStYZGQNI5X4hhahFxipyep4ZYSfSU0O1LpABolcLXfgQMyWCMXOEtb7Ri+I83SqM03kNq+SBeqRyxAcwAN7UglgWsaeP8NyV+NWuA4J47Rglgq93aQLIDlzmIsrlnqTWaKCIHTLNHdnyMC1FsVFekR8rkNjBqKshCn2coRUh0wUZFmh9V16mamiDyungSucThN/j7k8IF4LTiQhq+l49EY/iLwErA9SQJndQ2nLdFNCFyzUvm92+wlIo2iXkgNt1G2lOEvwjkj+BGSwK2PFv0bOKQ1EYPTip+TwDWS4GDT0V/dh9hTGekpfhw4X4RxRcBnMN1Ban5yMn9y4AXlQZ8KJwROkwS3H7JYHmmi74XU8OfkmYv9MiREgohct2GBs+3cNAuSUyocCVwjCe7es1Al3tK3S2MiJdqezNLsvkKhjFAhzgZk842aD6G3feRms6YIVE8InOYXg8o4LnccDhcCtzzw1lSGQFwFxNpIzX9JX2xXh9UaaiRwv39aLRd3F1b7FQsc/steKkOCM+AE0fxktthZzPKV0BRNOuR/mlm+A06zl+D3lSA9zl0+JM+GERLiLVBeI82I1dF62URNru8u+1MeGwJXqWh+F3hLiqB8nHzmVvTIyowjDII0I7bZQFXpD202yB/NNoaXgdFcdehM5KTRzBm/jIjOYOomCdwIR9Dlq31Nyor18Y+pbQxAXuax5/pKovkbLi4SOyI9IbBGav44q9KazQyfhz/N41Dzj+FaKw5+5oeK5iezJLOY1shwH7yHQZDmwtvOYqGkC7S22h9T+7Qkojt3Rkt6qLwMFzk+DjwX1Fz4PI5RSUxzsMwi/WNqn5bE+rTmhyqbb7W8H8N98CYtQRK4cZIKtOGEP9mbvo7SMMRee16Smv+0eeLycNGi/Cn5lBk1EbZjTYRmxtvz9o+pjagSHBpXRPMTriwSL07FqT4kIiTNghFSfBV0Kb2GBQ4QcQbZtmpTYyM92hFyfxhugnehCtIsGK8K7DgCtzpEv2ELpEc7ztoqw0EkWcKzQDZ/vBM1xxE43s8gSALHB0JHC462ybaPmvFe1rfVk8DpdqiqPxgEroElOP7TZbgEGO8scOiB7+O36pFRUJOy03+bPxC52k9h6amPDwUHyGXbn7dcViRSrI5cWUf8dzz/lFCTMtCKwLF9rtq+mWWl3SJ/DDcRe5YvGp5MkApakzLQisBBtkULYxY4kJp/KDhTJEawwKH529PYWQQ1KQOtCBwgypezETdKEQWnAJccRuupOKHh4C7fpVmKgb7NBJMswf/ih/gV3oL4JD5vu9DrJIg6l4Ba/ZRMkOhekzLQlsCtj+ylCoHDfFY9EhwwfGGfooHr0xrzebFbvKQv82SuQkx4+t2kLnrePuOvFvsFvgHfA+2D8AWwcBm7wE10vFxNykBbAqcyudHUaDWOHnNIub7QHSgaWrQ6rt52b+rA9ppmVZt/k5cP//4SqOT7/v379A1Dz9NFzKijqGh1MnZ4QaImZaAtgQMwRuN9xiA95s1po7rDT8B/xEhFK5b7pdpceZYh1UYt5W9vsvZXF+JX8iqJ8HRg38G482s1M95aSWjymNuzfqMmZaBFgYPzEukzlqQnjZmpusMrwFiDtwgvQ50qIOVGNqrKS2PNsnaJ89Vf01dYRl4sa+KVIO7ZXhc5SzQ5me2yac4kqUkZaFHgYi+oQE/aL4G76No8nV9kRTTkwmoDx2Ht6nRXr7tXdKzL3mukb3dqL95DUwWOalIG/smebQkcEPU5W2i1PwIHB3B1WOl1rdqoaVm9JfJeocVuuq6RHsiA9iaz5DRZLeualIG6vaiPbdWqAu9YjMLroIyK9LAdF7i8zDfZ5m1HOT3O6lqTlzuk237fv7vmt4ogm7xDx3vSIKmlIxzw3IqirEnZ6d/G/Gb7GuKNl6PJDgcZjsXx8/gpIqEe6VqNl3t2T+au6zPxkB7ElHlR41QTqUG8yiIWOAePnoH3tDwsxR1Wpe1y2z5SNoFkbrlfTpKg0ERcWQTUzImrS+gFrnkctFGBAzDmInrSF9KUcyrRFwaOmnXycUjWbttfyuZQ62Ccjp9GX0NcqXBoZjr1vh2twGkOnTEtcMqIq/VI8KTJ5ojTJKw2+ZoJUtqqlE1LZvN0Pu3bJaKal9Th01c/7Ctwqfnoe4zHCVJjIe6qCyYC/DXhkFalrXafQVI2M52h7VNlk1w381zuKkhSA5+Sp8mHenlsCNwTBG5p8tjANqDxIpx67o7wKZ/69mnCXC34aPCSRLdHJW0XyiYn4lj18ctaAOVPqdKkw+556uSRC1tqAdezJmXZS2L44OcOfB1i2rxCzZwn86n2jWNKq6Q23Ekkfa6lbHsqYqzjmxiRpBBMmRpSQZk0Dn5+S/8U62Ptp/nKyrIRpnpEgXOM6WT2tpvg7OdDcVAVleVtxKxukrITYMqlzyMHtQPfzyDb5czxmHA961L2vvuj/an6C9MQy65pHFOOZtTIFWPwFlVbR9hwa1L2RioeymhmdV7mquZKkM+CBvnnwZWj47TO6B+N4/pqMfYUS3IQPfsxFybwFlUbEnDp4Lt3GGXPJMKlGi26HWyOFLVowm2nTcD1rElZ8X38Ux4aAje3aMbHEm2gibTLR6qpoAw3ed2wO/Zxyv4ZaxddmF4qNQcT2ZGcaglNQsj29EebPKL+wg42xzgqyWzGyBHJykwYxdLxD75LTVH2Fbmrtg0QfH94NXVE0eZ0NklsugPZW2PPwi77gydQ++npr/U32/suaEeV2oVhbXvywDp4Tmkzaag9aY/oMZACQbbfQ6EVv6R+G3l9uQ+0u07/4Be1n4LyD+whKzK1+BokaQTAsFKttQMxbWC4yctVr87sT3pSeElYXZILqqYO9RjeCu4svV2QPWkqvwmBg0/a/IX8G3sQaxOhRlRpENirI1KUhVi6ZrfUCGUHWq77AnsHl/D+YdH9z9M5DBTVMJdw+m9T17GiJIFr1Lw0WBKuA5+HQHcjU4ssrb/CnxL58eyWGmc6g1Gsetk08NTEI8NVPH9qMEUdPRyjZamNBK65OJeMpNAqJTWkuUptsZTbDU8qihj0+ER/gjLsUFpxvvzeji1ve7rDFm6ixDvkt4jBY8XPhcDlH/X8EUu7tZrIy1zsKLr0YABEQ6hcj2qhOajqFPIS1SsyTZGe3fv+3UYmsFqJk1epXtR9yhtOp6xFfhPlrrFPi/J5hcBpdmtZ28zQBFw59eDDII0G45tX1qe1WHSrXYtpnKRxr7tXG8tMXh5RclY315JCaig2jR1ZSyFiQuDKtCF+L6NKNTyvQGwTmh5vqeEtqKJ0IocURiM9xJf0xbjGlT/la/qqLlG9ouN0Xt0A7TYG/JwErllHyXKubxPowRDmMM0Nszu6Wd0mID1HaJzxFDmY9t68y+UdOll2v4lsoQ8kCIHTnEZjPxWuCXFWrvTCPJ3JdNsvicnwgqi3k1LiuKd94i/R4VQW2Hg0XDiq8pnWrugUabw9JU/ORhVqaEsFIYH7+Tn9a6SQ2E+Fa+K60uTjfKYpYdCYl+omvtbH3giA1O3P6bNxO871nTw0kiHujtRB6gONguXCzlACp9E/0ycz9ITXdpzB7BDpmbr+ng+eZ40za8flZS6SGc/f7xBxP6Rui93CzWxePZo+6Hm/6VngmqlwFgqX94Raj5PdXe19l4lbNXGqkNRHacmyZ+oE6RHAnDFrx+HbxD7i8/c7QXknqUhyspQMaAna0xjkr5TAaVLhPqY8DirN0qfUn6Qh3KS5U4W81PewSQ/C+BYlOICuZMbhBmgMQ3Mx9dT9+QNNsfJzUUslcM0sEqtlL/sAj1/kAHuSN4RbNXIQ5y7biSbjO31odUSk+S9yR4yeIuSExslLp+INPeEZSY+gmSNysc+UwDXLXp7+s7j3uCdgxrtepfY8OIyUo0B7fU12j4F4KJQDbORNdsGhmO5FjitSo3AD28z1TLcONFfYYLHJXymB02eKFNP74UVZiEoMrrpssvKfkU3aaKmXWaBRkeRgsVuY3WgMz1dsyh4tYi6vQm0BV8eVWckeH806IuVRJYGcBQ693KimNE5NkT7YnrbCtHHMXRXqZm7pLcazsX0k6cIyNXyKe1mWIm6OASAvYWkYyG+mJoAwHZyqOT4QWuPs/AKqCFy7mecCYMm75a7SKDH1MlfJMS60i3mL8sW22q/UwzOHfb4X6SPGZU5+lfy2RKTvQtocrXp0PzTHZp1DqMBV4DoW6hwB3nKOHIsnhzjUzUjZiXC24kbCjVqasHFqGkbU9+lb1LuuytyAgXH5Q/m3NGvmyRx2ovG85WnRrBUCW039ripwmi33UwdStYBRrY5ZuTzFy0MdgfKK5orq4EvEGnPtKkzHKUedtQpCWZl9Hb9U8KGmdPLSNVZ/eyF+Tn8OuVzul2mWGhmxriFf1g/TqlZ7qwgchP3351wIpLYhyRIrxnw35YXS2eqwMrXMLIqPoxXj3D/TIOUjs3muO/QI4xwjRC1AS16uXqX8Id2P4kYcWoqBCl3zPYbQDc0urPSa7HIVOOD099fnQPSx+p17wONfn9bqLXd55JcnbZaXLzd6niYvvflNenCwj2zvaoJBB51aHVdvu7fraU0XLaM1k+ftMxQNagjTDx8OzA/tQFO1YKup39UErlsL3QTeTt/H77rMGZSMy7dRDpTBpVm8+a/3fLkc0y/i2WFgpK+jeX/FTwHxwuDZ5bt9vsc/MCahgEG6nzfR3KRVO/X0l8A1j4YerXb5g8DT3Z62Io/s/E5TwvSIdly+gQJP36dvg9lPuGFHt1sz7yWNkOXOcOIIow80EYbfxXp/CVy5bWzYGre074OAAOGd9nH4UEGomtL1lJLqh88xdeMGvyiFhNvreUtMx4nnaPNQLkYbNDbZ6lfuxy+B+8l09p67q3CtgN+6zbbL/fKX0smBeJP4GP0J3F5LMfU04DNhYyaeqoc71b1G80zn2qrab4H70dSNm6ownBFA6Xb5Tq7OyuS1G6SabjDZ8FeWFjWyMuhT/aMlva5g78ezuj89emwwrQtccz+DL8twNwFlgWytT2vYZcvDcrFfSL7v36Fo36fvJEuOxdHsNsMmXK/myhxMeqZjBhwihyZ197leMqAucN1pwYwHoWq9sbqFSjxZc9uTGd3QbL5qnHdaFzhN3aR/7qb7+gU4LyJjU06D6qxghkQ83NRksiSjDdlLUhOrZh3yusDBP9Mkzp1rjzAewSJdsHMaPuXztbnDgSHRneIr0RA4nS7Cb1W/YwzF5rhhdYuF9JTn6ZwX4+xBk+Kr8zU1Atf0bHkZ7kGwcxod8aA5+9cmiu9e0QKNwJW7emzC02w4dyDKn7D5FhvxuNPZ+siLcVbQ09HUCJx+Gc7nbLhpkWQJR05jpHzivBhnA0XZZwEO0Akc1PG1kQ33ewMEoyfyMldpvSxwEZIe+kv6wotxZqHJgJvpD83RC5wmG66RQcfoAy73Fjvx6JPZx/5DDQiGCWi2oDYy4CT0Aqcpfvm/NawR9WtGP4ha5OycMjEAeJuqUWiO0Wop7KYXOECzi7WRRMfoQPlTqoJIzMhJb7hn+3UxI4EmQaS9NG+rwDUrnWcLfgXdAXU+DptvTJAc1cVuoQYH4wHkn4e6NLVXdWsVuOYyHu/Z6o+szDjxjfmL5Khy1sjjaCaIdBQEaRU4ThZ5BBxbYNZJgwGvvRDOWp4QuaZEUscR9e0Cp1XKpT5UwaiCYwtMPTEkktlb+ma7JFfAKDb1quPdnmWXwDWTRVw+SNARYOyKoyFqI5vJlCSN+zpwcfOB0FhdLQkiEl0Cp7cG2UvthKj4xs4ps41yYPD2hmHQKtKuKzbdKXDspd6Joix43wLzBmlswMxnR/VeaPxT+JSdvXhD4PTfyGjB5+GTzTfmbWKEwFHlU7juxAB764bA6Te1dtqE0YJrIjH7EiOENI4d1TswaMXslsBBNZsb7ztX9aLFx/6DzTdmX9I4YUe1P5oF4Pp4k7cFrnkatIjL8kP5DZhvQt1qg5jJ7CAZcd/HbzWGGJ3I5kPiAbcFjr3UPuDMXubdlKMlmeHtqIYRow3NM+mhQu35vRf0EDh8+aLupXIR8ypEZi+rG3MAyYjj1N+b0Ow/bSkAV0MvgSsTXRFzrp50xmLHx2Uxh5I0jh3VbjTrI/UswdtL4PB2aV6g+OYtdQKH/MDqxhxOGjlPyRM7qm3QFP6Af6orUN5EP4GDifjROGqrn4kYPN53fKAM8zGSEQdHVQ0pxm9kb40lsvb6SDX0FThtjV/etsXmG9MMSeO4mJIG2vS3pG+Qs6/AAc00Yi6ByecBMs2QhhA7qk3o0996r//fIXCahLi4Qw2Hgs03pjmSEYdXphpeDELz7IT8444T/u4QOBFq+NcINegOW40EbL4xDRNjKZ0lWd8FpuChOYQe/unxjjMY7xE4bajhKdJQg9q6gBHJAsc0RRpLz8lzXnJRMgFNeGF+n/rfJ3Da82zaDuwKG6vDis03pnliRPE5qhK5bg/VnWf73SdwQHNHWIS7GvCCFYVD2Hxj2iAGVTrb53esNAWJpr84YBf83QKn2dVwp1ccAPhIQKZtvqQvRRnXtPoF3Yr/XeEFibsFTrurIaoyv+VPOU/mYhSywDEtEUMrmX0ePtWYiw+aA2EG5WzcL3Dmru0p0ixl841plxhdpHGHItKKmE0rathS2BCB0xZQ6rn3NQDw1nrmGCSBi7PQiH7z6aB1sEECp13/uye92F9cs0Nqw5HJNE4Ms3S2PUWXiZU915N7780OuWCgwIkI7u87AGNI+uXsEOZ4pGE2T+ZRGXFmc9GGCpw2B+858FcNnwrIHJsYaUlcRpxGWB7YTTBc4CJM+k2yhM035qikwfaSvkRixGlV5RHXcLjAAZqkX2htuA+CwwvMCRiTEdc03x5c3H9I4LTBjlBX4ji8wJyG0Rhxxs034CGBA5rxjlDDqV/HLxY45jSMw4gzbr4BjwpcJEYcXp54haqhVh15TOZYfElCNuJsmG/AowIHxGDEcWly5sQkIy7Ngi1sYcN8AwwIXAxG3OfhkwWOOSVp7L2mr2pEhgVtYUsjGmJA4ABL6usI4Bfw7nrm9CQjbpcFWNjCnhdoRuC0/nP+Gcg+YT64nukESeAWu4Ual6HAqgtoRuAAjREXSokR3p7FdIWkcYc8oBIjpd1FfGMCpzXioHrq196C/VOmQySBW+6XanT6D1hqNdEADa7gGxM4QGPE/W9dHvw+PkPET+XAqo4zJnMqksYdiyCCeHnZLNsrtrSbc/xMCtxPVjTrxInb9Rmc38t0iyRwq8NKDVCfkS93NbkAze5nNypwuONVo04c7vjOg3Ccwmv6qkZVdZAxmVORhmIARwuOs6hlWOBgW54atYb9TRkR+0/PQ4rJdIVkxG1OGzVM/USzVAf8v/Jk+Jwd0wIHidue6vftbUFzjCH2T5nOkQak10m/2sP5BhyadRPmBQ7QxH39TBlZ7pcscEwXSWPS13yRNj+vMC8RVgSuPOT1u4d3/epZykhRFpwgwnSUGJPJ7OPg5QH42pX6YmNlpd6KwAHZQpcykvhU75cTRJjukobl0/bJu1CDPrZgLdfClsCJDJdGyoiwQv1xVNenNfunTHdJRtw28ywNS7N+BdNnb0umrQkcXLxvTY5y/u7NVuH3/TsLHNNdksBhlKrx6gPG1wSLAvej3WUGtd554KjyAVpM14mRuZk9JU+nwpM8U91GANtenU2Ba/O3Zx4cTHMoeAGO6TzJiPMlIS571a3LW94FYFfgAM0Z+HYSXsyCM+CYHpAEzgsvVZseO0JmhXWBE47qTOeour0J/+PwwQLHdJ00PuGlZqXbyz55qUl8+7uG06o+YA32BQ4Sp6tH7PgJqnzEDNMPkhGXZIkauE5CmzRWfI9REGUMgQO0LcyXjkZU1RZUJtN9ksC5XFxE75yOVWRoJIHT26iuRlS3py37p0w/SKPU3WOhs6JZ8Q0sj9adU4mxBK5FyN1M/eUtqEyfSAPVzRKYmpIhcN1GLL0xnsABWkfVtT2qXKOc6RkxUJ3c0pB/HmqTHRTO6YgmzagC91OU2oiqpX22w4A3oRo01THEZDpLErjV0a1lOFFxo5nWa6HiWzfGFTjZ7FqbwdGb3YE0S9k/ZfpEEri33ZsawS6gJTls/PPgxxY4wAXDtQPfp28WOKZPpLH6nDwXpStWQv6uOWwhe5kgl2UCgQP0S49ubG/gFF+mf6Th6simVH048d804cRpBK41eOzA8TRcRITpHzFck9kunz6xVGw/by69YWobPSurPyYSuDaZ/ztegkwbFvsFCxzTM5LATb/rviWKOGGRtMkEDtA76rOtjdLs/fG643MCmb6RBO7z+KkG8UTQ1gsRM3q6CT2lwIlQy5NG76fNjGOBY/pHErhpj2jQBg8nT5CYVOCAlsU4dJb6wOh427+pEVMdQEymyySBWx6WahCPjjLVFNQAp1p6u2BqgXOva3gNjukfSeAwdNUgHhltZooDeRHTCxygPUZMxJXtl4tqAq9BFjimZ6Th+rqb4ijotoWmKbLemnBC4AB0R62DQLE8ObrGfR4/WeCYnpGG69t+9M0MULe2wMKkocILXBG4n1wfYBY7HMbtKS5WzvSPGK7JBAKnLZ8hAgvO1Ot2RuDwMtDuzoXGzZMxw8z7fM8Cx/SMJHAjr8HlS02aF+hCuv4FDgkcUKw1xyaCYyaOZGX2tH1Sg6Y6hphMZ0kCN+bpM/qkEGc2XF7glsABrR03YjL0W8qZIkyvSAI3Wh6cfhsSDJGFW7UdAecEDtDucABHKwTKcQamZySB+zp+qRFsE215XcLNciKu8AsuChyQvekWL8c6iYeX4ZiekQRuhL2o4oQ87UL56MHAnnBU4NrCz+AIS5hFWTwnz2rcVIcRk+kmSeBsVxMRlUJ0Cb3uJIU04arAAUWJ10KtKyVHOIuLq8IxPePGcj24lu0K4tyoKRLye8JhgQNaqq+MkGjDXqoVoj9ll8p/1Fj9ALM/qcfsnhzYkqYKyZu8vlk33BY4ICum6lmOpQ5kVapqrH5MEm+R2k/A2l+BtQ8wq0T/JLPl3tpO+zZfCnbG3pWE3jY4L3Ddnr9N2zg5JWzE9eJFhiTlD9F1TW5mz9vnl+QF5sbr7vVt/7bYL/Bf/Bs/mW/n+K36hupftV2CKYkOsXdsYKnfRglOXimkDzwQOKB1k4PN1U0Y/Jh1agBVxxPzIjSS+ElFjJ62T+i3xW6xPCw/j5/fx+/NaZNm6SE/nIpTUeKBaR4ZfgjmZY7P4JP4/Pq0/jh84HvUMbUXsavdgPxJzKROsLQA1xbrc+qozw74IXBAV3za2soDHyH4ixdNkR1yVhwI0Pv+HVoGI+JQHLIyM7sYBE08FsckS3CJt93bU/J0FbvaLUVINNzamYGtGanTlWu8F94IHNCaP20zB0esxEWucVUFOSvLa/q6Oqykoo18Wh2svF2+g9ip2stNsZO3HQnRXjsZcK3qtpz+aJv+8EnggOK7ZbPqk631OExgMX/inDmXVpOIwFJb7pcQNdhTqnemBvwyzG3Yjxqz7tKQgEnNxHMx/I4pWzPt8XP1GU/gmcAB+uqYtB5nKa76dfiapTHNmYtAkF68pC8wl/b5fmRL7S7AL4YP+0vpqg0JlWhdMtuejIYXirItqiBqWFpbDrIE/wQOaDOeRe6Ihfy48qeER6bmTG2EBcOqHJCuoclfxy8YsGYX1GwDSgebTtSdPzfkV9NCIlqUzN5So6tveWt2vdWFIHvwUuCAVo37a2WfA5wyYRrURlgArE5+MnykvXbIPdO1JuC9QqBFHJzadW3spe1eU7YlMRo8bcs59VbdAF8FDsg/9L6q0DgL+1VFRBWOqhxb1aHmKeUMoUkCPifPH4cP+KG+61oNaM4u3y0PS+W6Vhte7Q3fKEoWprP1aa3a+TDask1B4Zn6qW6AxwIHtMUcwGJtfiF8fVxjVImx5e/0qE5vmvDv+/dtts1L11PSHwQsne/j99Wgq/aDbxQj0OjWBZGD1aZuby4WQeoPvwUOaMsdAW3Uj4PX85Q++adx1flMM/w1fcWEdyceOg6UQbdfqn649IzsJS+Iu6XEN1MxnzLRZ5iC+YhVZi3Be4ED2nKAQRtP6PP46ZOvKicwzQowVFf0XsgVunk6l91y7aVa77lGeo6vu1dTFnexabcP/Mnm7UAIAgeIvVxtNraFQqOwfVzXuOqMJVMFrmiSJcG7oncBRhD6RB31Tb107TT3KD1TmN5ZYSaM1nY8AOjLTqybCETggPLYHgOysEqKiaHiqk7NBzk/5S3RjMXbHnJsMtYWIg7FYXVciSqnVZmT3egC5c2k4i1l6hXVlYeQeLCLvifCETgBvNo6snhywxqHWSF2C2FKTD4Z5A3IeyBj5JLtoe6V0QMyh04906rSXfp5EsobSGd4oGVpYgy3b1QQmaT2q8mOibAEDijKbN6Sh22hvBJ8HLz5r5PhMihHoJx7kvhfmpPzZL46rHiJ7RFARNCBTmSWyIvisabzNDO0R6p9o4JQN7erVw5AcAIHtJ/nIB6hhRJ9mA9vO9qTfxmU1WFqkPLLq5egi8Jek7rm8m4q7wCDbn1aq00sIzzcCy+PmK77cfjAnah7egzlqcieWlwcy9UVp0KIAkdoXWKwczQXLKbtaavSrKpjtDpwh/HyVZcvpHEP+wKqKnZT+b/rwGWgb1sNOvk4TLH6nfSI3/fvuLS6j4chjvtrC8Q9h6luQLACB7RtywdF+qKF5GxMBn1UrsbLmL6w9oEq8Vv5beBGGGt4peMqp5LjBqNCrtCpQvbahysf5b2sfQO9uqCnZtdPOwKm2dzjjQo3EbLAAcW6dasDbHVY7OpzpnEoDp/HT+Hd0JBVvIzpbl4+T3/ynDzDUsO3pVl6Kk5srE0OSA8M52ssgh6TYE2qLv+48PLbC+XPz9+DBw2n2JRDqlCUrSEFzAInT2s2iMAFDhBbHVrSgMWSnOWI+KUULdwNGF/P22eRzXQZ8Wfih/jVPJlDE/FJ2GgY6Lt8B0XjZTVnASN6m21XhxWemnqsVb3r4OVjZJLDXrNUZa9j0Q30q3TlMIQvcIBIA/5Pv/oA5h/md3RpAeMrpzMHMJRhBVyI/8UP8SvWMn+RFdk+22+Om9V+9b57h+S9JOf32Ua8wOQ7DD9cpIvlfvl1+IJJjkdv76F3LLrhlR9MKm83ohA4gbw9Oi6XIUxnyTEYk6Fsr7UD/melbKKbiEbgCF1P3b67ymCMALGlp90t9br20QDEJXBAR+0EUGzOj+nxMwKDKCDWMbzHWo1xB9EJHNCxaxUU0dVoDHhGOMhb9/AIhrXDtD9iFDiBjg0rRBvJwAyGJYhUgbZ4guWMKMcRq8ARuu15EXkINL2bEQ7wql60prmBYtUl4kWXqAUOgDfasSIroulsyjFchTDc2vOfRNwsjT1uFrvACZQ/+bJ14yoIUy68KgsMv5GXrRUliGKLAmc+scBdIOqed7wMYeqv9jGb+gx3IJZW2lfc2O2oggWugptvxaetjWpLDEZPiK1XHaFSOUTZ26iABa4OsT+/PfIAcq4cYwKUVBGke2Syk9EAC5wOWdFtysFBsHHuKoOhRZlmHZmbICdvtoEFrhXduUWgGFVhFbBnuAaRlN6ZsAmbTpzvx4ZbC1jgOlGUHZWBJWHrRZtFybAIjL3O4D4o4vs89jrBAncbMNO6HQRQ7PLjhTmGIYiF4E7vQSySxFHv6EGwwPVD2VUAXZEX5hgPQ7xNOzLPiZzj1h8scHfg9oIIBt9sW2751cq4G+U+vz26eNn3TrDA3Y2bIS1QDMTod8kweqLPi5P9g2FggRuE8lY2OTF75vctowtC2tpPhLlQHJ7AK7yDwAL3APLbMVYQL2fe/8CoI+slbWLw8M6EB8AC9yhEPZLO3TOSYqSyNccgq63Xe3HGqxwGwAJnBmJh7vnGwhwo1ua2J07LjBMiQnpzrQ38j5bbeJCYAAucSZTJ7fiDIEbw95FXVWJBKXbF3Ez+EGRpMw0WOPMQo7mPzP2lxWMuGhww8lLskO8sw6X4b8M7rmyABc4Wik1ntdUKxWYvPrEwLJSHXCy0dRb/UJTSxua8HbDA2URJe276yRw+JsrdsEHnNYoSL7Ze3ijI0mYfLHBjQDitPUIQkmzQ+Yg7TDaKkIq1NpY2+2CBGw9iL06P1CdFNui8QF4W38deS65EUf+D314jggVubJQnSoPq96oHYfqJtz0rnVMoSmGV939d4Tm+pVyTcnywwE2E/vG1M1nppsf9uiYW2jhWPh1Y4CaGSJ3rLo/eICvd2CjpMS3S/nY3KLxRTuqeGixwbiArRL25eww6EEqHv+K9iraQFSIkCnvtHl2TJhsX2nUELHBuYYBBJ/jfJn/fidVrDsw9CBhraQaF6h83uJBNNgfBAuckyKAbMMdATLPi+8hm3V1Ad4lg6IBXC4i3C5tsroIFzmmIyhMfA5UOjlX2kuSfB1HFhC27GmCp7XN0jtj9fpcHeqF0RbkQlttggfMDIo90ubt3ka7K7GkLN7bYnOI17rICXrx4YfROutYQuva+40JGvoAFzjMIuwNKd6uY8E0K4265K9ZHYYMEat7hrVBuT8pMe+DdIAhzeME7TPwDC5yvUMtGPWpt9iG84Ow1Ff5smnlq4pWnAs64kLO39CEbrUIRp/7Yc6VSf8EC5z+KcnDgr4v/NjB8YLYI1duehKHnSOZdXuJmpGkGb9GAdVYjGg5jbXvChdQVGd6CBS4owIqB1ymigQ/7sG3MnrZC+MA3oX2Swu6D6IAPBxNhP8qvKjYkYWcVE+xZpWMQ8f0iqZB3U4UFFrhgIVw2mDlQB5u64C+FV75IxSoki1q4YIGLA9KNXe1hp9TmeVRUZlqasfsZCVjgYoRwA6F3nwc4s4ZX7pzif2IZUXjQia+RE8aDYIFjCKg1rw9h4vkqeTKx+X0nNnLsc85tZgAscIwWZLTYXwlWuiJ8JGQyqUVqGS+iMdrAAse4EwVlaRCF9hFF6hkFOg3k09L3gPlqL79cOJjyirzfk3EXfn7+Dx3ti4ceBvBFAAAAAElFTkSuQmCC=',
          width: 400,
          absolutePosition: { x: 100, y: 215 }
        };
      },
      content: [
        {
          image:
            'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABGAK8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKxviH8QdG+FPgnU/EXiC/t9L0bSLdrm7upm2pEijJJr8qv2mf+Di3Xb/Xrmx+E/heys9LicpHq2tI0k9wAT8yQAgKDwRuOfUVrTozqfCj6nhngzNs/qOGW0uZLeT0ivVv8tz9bqK/FT4U/8HDXxh8L+J4ZPFWk+GfFOkFh58ENsbG4Ve+x1JXdjpuGK/Vj9kP9r/wh+2j8J4PFfhK5cx7vJvbKcbbnTpwMmKRfX0PQjkVdXDVKavI7eKPD7OsggquPp+49OaLvG/a/T5nqdFFFc58SFFFfLv8AwVO/bt179gn4T+H/ABBoGh6VrtxrOpmwkiv5ZI0jXyy+4FOc8Y/GqhBzlyxPQyrK8RmWLp4HCK9Sbsltr6n1FRX5h/sbf8F1fHX7S/7T/g3wHqPgbwvptl4kvvss91b3U7yxLsZsqDxn5e9fp5VVaU6btM9HiPhfMcixEcLmUVGbXMrNPS7XT0Civl3/AIKB/wDBVLwR+whBFpc8Mnibxpew+db6LayBPKTOBJNJghFJBwOWODgd68L/AOCYv/BW/wCIP7cH7Vt34U8QaJ4b0jQv7Knv4Y7FZDNGyMgUF2b5hhjngVSw83Dn6HXhuCM4r5XPOY0rUIK/M9LryW7P0WooorE+TCiig0AFFfGX/BQP/gsr4O/Y11yfwtolgfGfjiBQ09nFMI7TT88gTy8kMRztUE9M4zWN/wAEi/8AgpR45/b28aeObbxZpug6baaDBBPZx6fG4YeY7AhmZjuwB6Ctvq81Dna0PrnwNnUcped1aPLQVtXo3d2Vlufc1FFFYnyIUUUUAfmt/wAHHnxhvfD3we8D+C7W4lht/El/LeXyKcC4jgClFb23sDj2Fflr+z98EdX/AGj/AIy+HvA+gtbRap4iuhawyXDbYouCzM2OcBQTx16d6/RX/g5U/wCRh+FH/XG//nFXx3/wS6uWs/27/AEsXyyR3FwysB0P2aXFe5gtKOm+p/ZPhrJ4HgN4zCpe05ak9f5lzWb9DF/bU/Yr1/8AYY+LsXhHxDqelaxPdWaX0NzYbwjxsSuCrfMpBU/XrX0b/wAEAfinqHg39su58NRTt/ZfirSZvtMJyQZYMPG49xlh9D9K+N/iZ431fx9481PU9e1W/wBY1Ga4kD3F5O00hAZgBlj0A6DpX39/wQO/Zm1JfiFrfxl1a3ls/DOg6fPZ2E8iYF7KwzKy+qoo6jjJ9q7cRBLCtz3PqOO7UOBasc5mp1Z00rpJc1Rv3bI/Sz9pv9rzwF+yH4NXWvHGtR6bFMxS1tkUy3V6w6iKMfM2M8+lfFOsf8HHfhC31YpY/DbxPdWStjzpL2CN2HqF5/U1+eX7a/7Tur/tbftE6/4s1O4ke1Nw9rpdtuJjtLVG2oqjsSAGPqSa774QfsH+Fdc+Btp4t+IHxZ0D4c3viO3lu/DumXsBka/iRivmyHPyozDAx9a0o5Lh6dJTxD1fY+KybwY4ZyfKaON4slOdWrZcsObRtJ2jGCbdlq3sj9Yv2Nv+Co/ww/bQvP7L0W8udF8TiPzG0bUwI5nGTkxNnbKB329MjivAf+DjBN37M/gr28RH/wBEPX5N6RrV74B8XW+o6Tfva6npF0JrS9tXIKSI3yuh9OPxFfoj/wAFTvj8f2nP+CYvwZ8ZyhVvNV1MfbEU5C3CQOkn/jwP51FbJ1hsTTlB3jJmeN8I6XC/F+VY3LpOWHrVLWerjLlbtfS6a7q6s0fDv7E3xk0j9m79qrwT4312O8l0nw9fG5uVtYw8pTYy/KpIyckd6/eL4A/tp+Fv2mv2eNU+I/haLUP7I00XSlL2HyZWeBNzDGTx2Br+dvTdJute1CKysLW5vry4O2K3toWllkPoqqCT+Ar9hv8Aglb4U1Twf/wSi8Y2esaZf6VdZ1mTyby2eCQqYjg7WAODWOcYaEWn1udf0gOG8ulCjmUpfvuaEEr/AGG303+Z+Qfxo+KWqfHL4s+IvGGt3L3Wp+IL6S8ldj90MTtUegVcAD0FfpB/wQc/YW8eeB/iDY/GTV4LCx8K65otxb2EUkp+2zq7Jtl2YwEO045yR2Ffl9LGPIHA5XBr9SP+CI/7f3xI+Mfxn0v4Wa5daPL4R8PeHJPsccGnrFOgg8tIwZAeeDzxzWWMptUfcPa8WsNj8NwxKhlcYqlGNql91BJL3fPRH6bfEL4l6R8MNHS81e5MQnlW3toIo2lnu5W+7HFGoLOx9APc4FcLo3x48U+M9WvYtE8HafJDp7mOVLrxDbrdxt2EkMe8xEj+FyCO4r5u1r/gpJ4D8NftWana+J7bU/7Slmk0nRtS8tGstDt93lrJyd26WQMzOAcLtHTNfJH7J3if4ifA79oT4s3NhDqJ1XT9D1ebVwVLL5gy0UzcYJ3kMp7g8cV8hiczpUZqK97v5H+d2e8fYLLMXTwdGn7Z8zjUtf8Adu10ml8z9W/D37QkKeIbfR/FGlXHhTUryTyrdpp47izuJOoiWdDt8wqM7GAOOmaZ+2B8XpvgJ+y/468YWyhrrQdHnubcH/nptwn/AI8R+Vfm7/wS6+I2keEtB+Jvir4pa+0HgbX4o7Ce51GZ3Oo3zFnYxdWaULySvIJHIr6R/aU+K0HxP/4JUfFiODWV8QLoemy2UGqA/wDIStvkeCc4/iaJlz/tKa6svxMcSlLbXbyufVeHee4TiOphqrp+zlKceam3d8jmoqXdJ+fc/E3VNWvPEmsXmp6jcTX2o6lO91dTSMXknldizMT1JJNfsP8A8EOf2GPHv7Lum+IfFXjC3srC38a6favZWazF7mFQWf8AerjCkhhxnNfj5prm1kjmXHmRMJFyMjIOR+tfsF/wRk/b9+IP7Vup+NrHx3faXPp3hHTreW0NrZLblASwYsQTn5Vr6/NsO6eGi47H92+PGEzGlwzGOBUPq+ntL/ErOKjyfN6n278RPi1pfw5lsrWdLy/1XU32WWm2MJmurnH3mCj7qKOWdiFAHXtXJaF8VfHfjfT3vtG8O+EjbxMyNC/iEXEm5WIKl4UZFbjkZODxXyp4Z/4KG+DfF/jbxp4VnTWrXx/4zt7izsdYkCizhZkb7LZRNu3IMbPmIwzsST0r5M/ZH+IXxA+CXwU+Oh0ZNWsFtdKhhvm2MpsLl7lYmk9pQhkyevAPYV8HXzWnTmoxXMtb+Vj/ADizvxDwWXY6OEoU/bRXOpyV3yygr8tl/XU/XLwX8eodU1m10fxFpsnhnW7wstvG863NneMvVYblBsdx3Q4Yeleg1+WX/BN/x94W+Gf7L3i2T4s+IzpHhrxjqiR6G1yzvMbiNfnuoMZYbGZTvHAKnJ61+h/7NPxKm+KXwntL66mhub2zml0+5niOY7iSFinmqfRwFcf71deExMa9JVFo306n0HDmfUc5y+GNhHkm0nKF7uN72ffW3U/OH/g5TONc+FJ/6Y3/APOKvkT/AIJN26ah/wAFCfhtDKu5Jb6VGHqDBID/ADr77/4Ly/sn/Ef9pbV/h3J4C8I6p4nTSYrtbw2YT/Ry5j253MOu09K+av8Agmr/AME8vjZ8I/24Ph/4j8SfDrXtH0PS755Lu8nEYjgUxOoJwxPUivoKFSMcO1fU/sThPiDLqPh/PCTrwjV5Kq5XJKV3zW031P0I8If8EXv2evCXiVtUPgttXnaQy+Xqd7LdQ7ic/wCrY7Tz2Ir6H1nwJaWnwv1Dw9o1na6bavp81pa29tGIood0bKAqqABye1dFRXlurN/Ez+asbnmYYyUZYytKpy7c0m7W7XZ/MF4v8P3fgrxfq2i38RgvdLvJrS4jbqjo5Uj8xX2z8PIv2dv2sf2X/BNx8TviTqHgbxR8NdIk0WWwt1UyX8KuZEkjVlO8kHA298ivo/8A4Knf8Earv9orxdefET4YPZWviy8XfqukXD+VDqrAACSN+iS4HOeGx2PNfm/q/wDwTt+PGj61/Z9x8KfGH2ngAR2gkRvo4JX9a+lhjo1YRblytH9p4LjXJ+LMsw1VY94TE0nduLUZRbTjNLm0akm9T9B/hV/wQp+DPxt+H+l+KPDHxK8W6nourwLcWtxELchlPY/LwwPBHUEEHpUf/BU/9iS1/Zw/4JheH/D3h29v9U03wLrwvZZ7sAzOlwXV2O0AABnB+ldh/wAETP2V/jr+zRpWsx+O0ttD8E6n+/tdCupRNew3JzmVdhKxKcDKknOQeO/3P8Rvh5pHxY8C6r4b1+yi1DRtatntbu3kHyyxsMEf59K8ypj6say5p8yTP5/zvj/N8BxJT9vj3jKOHqKcXdNNarol7yTa7H8237P/AMarz9nz41eGfG2nQJdXXhu/S8WGRsLOo4ZCe25SRntmv3H+Cf7aumft2fsO+OPF+maNeaCtvYahYTWt1KspEiW5JIZeCp3cV+cf7Yf/AAQs+KXwb8T3N18O7R/H/hWaRjbxwOiajaISSEkRiA+APvKeeOM19hf8Eo/gF42+GP8AwTg8eeGPEnhfVNC8Qahcam1rYXcPlz3Ae3CpgZ5ywIFb5hXp1kqkd0foPiznHDmfZdhs7wdWMq8ZQj8Wqje7Uo+Xe3zPxUafbFnrgZr9f/8Agjh/wTE8Rfs+eMNC+MGo+KNI1DTvE3hvMWmwW8izW/2gRuuXJ2nAHPA5r8/R/wAEpf2ixbN/xabxL8q8jMH6fvOfw6d8V+8X7JXhjUfBP7MPgHR9Xs5dP1TTNBtLW7tpMb4JUiVWU44yCKwx1duKSe5fjPxzCeVUsJleIhNVbqootSdtLLy1Pin9on/gnx4c+N/w0u/F194hk8I618Oze6b4j/0YTC8t7eWSWNwu4bXMLKVPRgwyDXxb8T/2xvHnxG8YHU7fxBqui2lrHHb2NpZTmAQwRrsjWTb/AKxtoG4vnJJ7V+w37Sn7Odz8Q9P1250QpIfEmnNpmu6VJMYYtYg2kI6yAHy7iPPyPggj5WBGMfEfhP8A4JLeAodHnHinXvito+otujihk0KJfLcejRrKko91YA+3Svis0y2tVqKWFS139T/O/wAR+As2zDGLE8OQSVW8qtpcrc9le/lfVHl+h+CviL/wU/8AA/hHTfD9vocNx4C32GqqpSygUTNuS+8tQAzMqsrbRnK/7VfSHxX+ENt8HP8Aglz8cNM02eS702xsl0yC6fpeNbRxQyyr/stLvx7D2rov2IP2I9Z+A3/CS2Xha78RWNr4ikSC88Qa1ZJZXEdvHnCWdsCzF2yf3sm0Dsp4r2T9uL4IXetfsB+N/A3gjRpLu7m0VrPTdPt8b5WyDgZPJPJJJ5JNejlODdG1Wr/Edk/kfovhJw1LJcXQzDNNMXUlTVRuSaUYTTWuy0Sv6M/ns0YNfTwwbgrTyLECegLEDP61+1f/AATF/wCCYOvfscaP42udX8UaVraeO9IitYUs7aSI2p2scsWJ3ff9ulfmXoH/AAS0/aHttbsmf4U+JkSO4jZmIiwoDAk/fr+gjwtbPZ+GdPhkXZJFbRo6n+FgoBH519ZmeNdSnGCZ/Y/jpx+sRgsPluWYmE6U0+dRcZbOLjqr21Pzd/aA/YX8KQ/D6y+OH/CQXGhN4Ut4V17SfI3G7vbMrCEQ5BjkkeNQcg9QRXx/46/bE+I3j3x9eeIZ/E2oWkt7KZDZWr+XZhO0ZhA2OuODvB3d6/W/9rr9kW7+MHgrxJY6I6Gx8VRr/a2k7xD500ZzHdW7kYScbVBD/I6jnBwa+PPD3/BJTwnPoN1Hql38ZbPXIFPyjw3CYQw7JsLrKOP4X9Olfn+ZZXXnVvg1ZPV69T/OnxE8Ps4xuP8ArHDcVGnUvOdpcrdTzT/NaHCQfAnx1/wVJv8Awv4q06PQdH0HRLFNF1mVZUt7fSHhJaV44B2kRlcBeM8E8V+kX7EnhSHwt8EVazEo03UtQuLrTxKuH+y7hFCx92jjVv8AgVeLfsUfsQ638K/hffeFXl1PS/CuuXxvtWm1COOLU9YQqqi3WFGdbeIquHLMZGyQAoJr7BsLGHS7GG2t4kgt7eNYoo0XasaqMBQOwAFell+D9jDnn8b3P0Dgzhl5VhJYjF/73X5faa8yXLeyT269Dz39q/8Aai8NfsefBLU/HHik3ctnZPFa2tlZx+beareTOI4LS3j/AI5ZZGVVHvk8A14W/wC1V+1ToGmQeKtU/Zt0K68Mzsjy6BpPjNLjxTYwsR87RtEttLIoJJiSXPGA3FH/AAWE0fVNF+F3wy+I9ro974h0X4O/EHS/F/iHTrOIzXEmmw+ZHPOkYyZDAJRLtAJxGcdK9M17/gpT8A/Dvwkt/HE3xb8CzeG71Ee1ntdViuJbsvjbHFBGTK8hJA8sIWB6gYNegfXHoHxA+P8A4M+D3h/TtR8a+J9A8FQ6ptEA13UoLFncgExje4DOueQpOMHtWncfE/w3aeF7LW5fEGiR6LqbRpZ6g19ELW6MnEYjk3bW3fw4Jz2r82/i/qfijxR/wVh+IU91/wAKHcXnhHR5fBKfFeG7WOTS3jdrs6evEe7zz+/GPMHyAgLXG+OPgBZ/8O6l8K6v4s+HvjPwb4h+Pmj/AGWw8E3Vw+iaHBLqEBm023kkJby1kLnCnaBIQMCgD9Qfh9+0V8P/AItahqNp4W8ceEfEl1o43X8Omavb3clmuSN0ixuSgyDyeODXnH7Kn/BRP4cftZeI/Fuj6J4i8NR6v4Y8SX+gQ2Ka5bXNxqkdoFLXkUaNuMTBicgEDaea8W/aQ+Cfgv4F/wDBS79lG88G+GPD/hS611/EWhah/ZNjFZi/sV05ZVglWMASIrqGAIOD0xVT/gmN4c+F3hv4g/tC2FrpvgHTvHumfFXxGltbQ2tpBq9tZtHCyBFAEqxFNxwo243HpQB9i6N8bvBniPXtO0vT/Fvhq/1PV7d7uxtLbU4ZZryFCVeSNFYl0UqwLAEAqfSs7SP2nvht4g+IsnhCx+IHgq98VxSNE+jQa3bSX6uv3lMIffuXuMZHevzg/Ym/ZZ/4Vx/wQ28WfET4ZaNFqHxt8TeG9ensddaFZ9VgX7VcoLW1kOWjRI1YJGhALYOCTWj+0PZfsuz/APBIKxn+Hr+DF8XDRbRvBE+jGD/hK28S4T7PtK/6Sbw3OPND88vv4zQB+oV80yWUxt1jecIxiWRtqs2OATzgZ9q8j/Yd/awT9sP4It4jn0Z/DPiDSdWvvD+v6JJN5smkahaTtFLCWwu4cK4OBlXU16F8KjrP/CrvDf8AwkeP+Eh/sq1/tTGMfavKXzunH391fn3+2h8adS/4JZ/tV/EnUtAt57i1/ab0NH8I2MUZdD46hMdmsYUdPtEE0ErHv9lfNAH1j8AP20rD416t8XNTubXT/Dvw/wDhn4hk8NweJL3UY44NUmtkX7bKS2Fjhilfygxb5ij9MV22m/tPfD3xH8MtZ8YaP428Jax4c0GJ5L7UbPWLeW1tCq52ySqxWM9PvEdRX56ft1fs46h+x5+zf+yP8O7afwbN4C0LxSV8Y33jITf8I/qGrvayy28+pGHkxS37yv8AP+78xog/Fdj+z58Dr3X/ANtzxJq+qeIP2bWXUfh9c6f4k8GfDyOe4XXIvNVrS8u4JAYt0bb0ViNzLIByAMAH1b+xD+3Z4F/bq+C+heK/C+saJ9u1bTl1G60OHV4Ly+0lGYqBOkZ3LyO6jrXa+Cv2j/h78SPGN34e8PeOfCGu69YbvtOnafrFvc3UG04bdGjlhtPB44PBr81/hIukwf8ABu1rMnwVi8MwfFKLwC9vqp8PRwrrUax3JS6WbyQJ96xrMMH5gRx82Kh8KfCqX4j2f7P1x4X8X/sceC7bR9f0u98J6l4Nlvl12+iQZmskUndL58HmJKsueSWf5hQB+nV98bfBumarBYXHi3w1BfXWpHR4bZ9ThWaa9Chjaqm7JmCsCYwNwBBxisbwB8YHl0nxLe+LdT8E6baaV4gn0q1uLHWFliWMMqxJcM2BFckthouxIHevkz/gnD+yl4J8U/th/tN/EzW9Hstf8T2HxXvrPR5tQjW5XQAltaM72qtlYZZGILyKA52qM4GK8A/aC8O2HjL/AIJ6/tJaTqCh7DUf2kvsdyqtsLo+taer4I5GcnkUAfqD8P8A9onwB8WPEl/o/hbxx4R8SatpQJvbLS9Xt7u4tQGKkukbllAYEcjqMV518fP2xLTwX8W/B3gjwp4k+Fkuu6prK22vQ654ot7WfSrMDLCO2EgmluZCVWNAuATlsDFeF/th/AvwV+z/APtu/sgat4I8M6F4P1O58XX3h2afSLOOze8099LuJGtpSgBkTfEjYbOCuepNfN37RPxD0z9rH9ir45fE7U4f2a/h9pnm63ZLpWr6B9p8Trc2rzRJNc3KyJNHfs0YeMRodu5DkjNAH6w+KviZ4c8CtKNb1/RNHMFo9/KL6+it/LtkIDzNvYYjUkAseBkZNc54a+Kl14s+MS6fp994O1DwndeH4tWs57TVRNqc7vKV8wQj5TalcbZQeWyK/P3SPgbof7Zn7YX7IA+IPmeItOh+CM+tahp105e31yYPp+wXS9JoxIRJsbKs6KSDivU/ifpeqeFv+CmXxJsfh/Z29n4gsP2dXh8O2lpEsawXC6hMLdI0ACqA4QAYx0oA+tLz9pz4b6f8Rx4Pn8f+C4fFjSCEaNJrVst+ZDyE8kvv3Ec7cZ9q7mvyz8C237Jt7/wRgkm1b/hDzr3/AAjMp1Wa9MJ8Yr4p8tt+W/4+/wC0ft2dgHzZ24+Wv0A/Ywbxc37I3wzPj7zf+E1PhjTzrfnf637X9nTzd/8At7s7vfNAHpboJUKsAysMEEZBFeZ+Hf2LPhB4Q+IH/CV6V8Lvh/p3iUSecup22gWsVykmSfMVwgKvkn5hzz1oooA3fi9+z74F+P8AptvaeOPB/hrxdbWb+Zbx6vp0V2IG4yU3qducDOOuKvaf8IvCmk+EdO8P2vhnQLbQtIkjlsdOi0+JLWyeM5Ro4gu1Cp5BUAg0UUAaOqeEdK1zWdO1G90zT7zUNHZ3sLqe3SSayZ12uYnIyhZeDtIyODWGnwE8Dx/FJ/HC+EPDK+MpITbvrg0yEag8ZXaVM+3eRt469OOlFFAG94Y8K6Z4J0ODTNG06x0nTbbd5NpZwLBDFuYsdqKAoyxJOB1JrjNC/ZK+Fvhf4mS+M9O+HXgmx8WzOZX1iDRbeO9Lnq/mhNwY5OWByc8miigD0KsjxR4A0Pxvc6XNrOj6Zqs2iXa3+nvd2yTNY3CghZoiwOxxk4YYPNFFAE3izwjpXjzw5d6PrmmafrOk6hGYrqyvrdLi3uUPVXjcFWHsRXP/AAh/Z58B/s/2Fza+B/BvhnwjBesHuE0jTYrTzyM4L7FG7GTjPTPFFFAEngT4C+B/hd4k1jWPDXg/wz4f1XxC/mapeadpkNtNqDZJzK6KC5ySee5zWV4L/ZK+Fvw38fXHirw/8OvBOieJLpmeXU7HRbeC6ZmzuPmKgYFsnJB5zzRRQB2ei+FtM8NzXsmnadY2D6lcNd3bW8CxG6mYANK+0Dc5AGWPJwKzZ/hN4WudLvLGTw3oMllqN9/ad1btp8RiubrcrfaHXbhpdyqd5+bKg54oooA0dX8KaX4gv7C6v9NsL250qY3FlNPbrI9nIVKl42IJRsEjIwcGuP1T9lD4Ya546ufE978PPBV54hvM/aNRn0a3kuJyVKks5QkkqSMnnBIziiigDqNP+HmgaRqOn3lromk293pNn/Z9lPFaRrJZ23H7iNgMpH8q/KMD5RxxVhfCelp4obWxptgNZe2Fk1+LdPtLQBt4iMmN2zdztzjPOKKKAOMuf2Svhbe/FIeN5fh14Ik8YCQTf202iW5vjIMYkMuzdvGBhs54616HRRQB/9k=',
          width: 150,
          height: 60,
          style: 'center'
        },

        {
          layout: 'noBorders',
          table: {
            widths: ['*', '*'],
            lineHeight: '0.8',
            body: [
              [{ text: ' ', bold: true }, ''],
              [
                {
                  text: [
                    {
                      text: `Nome: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.nomeAnimal}`,
                      fontSize: 10
                    }
                  ]
                },
                { text: 'Requisição:', bold: true, fontSize: 10 }
              ],
              [
                {
                  text: [
                    {
                      text: `Especie: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.especie}`,
                      fontSize: 10
                    }
                  ]
                },
                {
                  text: [
                    {
                      text: `Tutor: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.tutor}`,
                      fontSize: 10
                    }
                  ]
                }
              ],
              [
                {
                  text: [
                    {
                      text: `Raça: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.raça}`,
                      fontSize: 10
                    }
                  ]
                },
                {
                  text: [
                    {
                      text: `Veterinário: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.veterinarioSolicitante}`,
                      fontSize: 10
                    }
                  ]
                }
              ],
              [
                {
                  text: [
                    {
                      text: `Sexo: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.sexo}`,
                      fontSize: 10
                    }
                  ]
                },
                {
                  text: [
                    {
                      text: `Clínica Solicitante: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.clinicaSolicitante}`,
                      fontSize: 10
                    }
                  ]
                }
              ],
              [
                {
                  text: [
                    {
                      text: `Idade(meses): `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.idade}`,
                      fontSize: 10
                    }
                  ]
                },
                ''
              ],
              [
                {
                  text: [
                    {
                      text: `Entrada: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.dataEntrada}`,
                      fontSize: 10
                    }
                  ]
                },
                {
                  text: [
                    {
                      text: `Saída: `,
                      bold: true,
                      fontSize: 10
                    },
                    {
                      text: `${dados.dataSaida}`,
                      fontSize: 10
                    }
                  ]
                }
              ],
              [
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 }
              ]
            ]
          }
        },

        {
          layout: 'noBorders',
          lineHeight: '0.9',
          table: {
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'HEMOGRAMA',
                  fillColor: '#E12C88',
                  style: 'center',
                  bold: true,
                  colSpan: 5
                },
                '',
                '',
                '',
                ''
              ],
              [
                {
                  text: 'Material: Sangue com EDTA',
                  fontSize: 9,
                  colSpan: 2
                },
                '',
                {
                  text:
                    'Método: Automatizado / Horiba ABC Vet com conferência em lâmina',
                  alignment: 'right',
                  fontSize: 9,
                  colSpan: 3
                },
                '',
                ''
              ],
              [
                '',
                {
                  text: 'Resultado',
                  bold: true,
                  alignment: 'center'
                },
                '',
                {
                  text: 'Referência',
                  bold: true,
                  alignment: 'center'
                },
                ''
              ],

              [
                { text: 'ERITROGRAMA', bold: true, fontSize: 12 },
                '',
                '',
                '',
                ''
              ],
              [
                { text: 'Hemácias', fontSize: 10 },
                { text: ``, alignment: 'center', fontSize: 10 },
                '',
                {
                  text: '5,8-8,5',
                  alignment: 'center',
                  fontSize: 10
                },
                ''
              ],
              [
                { text: 'Hemoglobina', fontSize: 10 },
                { text: '14,2', alignment: 'center', fontSize: 10 },
                '',
                { text: '12-18', alignment: 'center', fontSize: 10 },
                ''
              ],
              [
                { text: 'Hematócrito', fontSize: 10 },
                { text: '42', alignment: 'center', fontSize: 10 },
                '',
                { text: '37-55', alignment: 'center', fontSize: 10 },
                ''
              ],
              [
                { text: 'VCM', fontSize: 10 },
                { text: '10,2', alignment: 'center', fontSize: 10 },
                '',
                { text: '32-36', alignment: 'center', fontSize: 10 },
                ''
              ],
              [
                { text: 'CHCM', fontSize: 10 },
                { text: '11,7', alignment: 'center', fontSize: 10 },
                '',
                {
                  text: '9,6-12,1',
                  alignment: 'center',
                  fontSize: 10
                },
                ''
              ],
              [
                { text: 'RDW-CV', fontSize: 10 },
                { text: '7,2', alignment: 'center', fontSize: 10 },
                '',
                { text: '6-8', alignment: 'center', fontSize: 10 },
                ''
              ],
              [
                { text: 'Proteínas Totais', fontSize: 10 },
                { text: '8,8', alignment: 'center', fontSize: 10 },
                '',
                { text: '12-18', alignment: 'center', fontSize: 10 },
                ''
              ],
              [
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 }
              ]
            ]
          }
        },

        {
          layout: 'noBorders',
          lineHeight: '0.9',
          table: {
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                { text: 'LEUCOGRAMA', bold: true, fontSize: 12 },
                '',
                '',
                '',
                ''
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: 'Leucócitos', fontSize: 10 },
                { text: '0%', alignment: 'center', fontSize: 10 },
                { text: '0-3', alignment: 'center', fontSize: 10 },
                { text: '0 mm3', alignment: 'center', fontSize: 10 },
                { text: '0-300', alignment: 'center', fontSize: 10 }
              ],
              [
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 },
                { text: ' ', fontSize: 2 }
              ]
            ]
          }
        },

        {
          layout: 'noBorders',
          lineHeight: '0.9',
          table: {
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'PLAQUETOGRAMA',
                  bold: true,
                  fontSize: 12,
                  colSpan: 2
                },
                '',
                '',
                '',
                ''
              ],
              [
                { text: 'Plaquetas', fontSize: 10 },
                {
                  text: '120000',
                  fontSize: 10,
                  alignment: 'center'
                },
                '',
                {
                  text: '200.000-500.000',
                  alignment: 'center',
                  fontSize: 10
                },
                ''
              ],
              [{ text: ' ', fontSize: 2 }, '', '', '', '']
            ]
          }
        },

        {
          layout: 'noBorders',
          lineHeight: '0.9',
          table: {
            widths: ['*'],
            body: [
              [{ text: 'OBSERVAÇÔES:', bold: true, fontSize: 12 }],
              [{ text: 'Normocromico, Normocitico:', fontSize: 10 }]
            ]
          }
        },


      ],



      styles: {
        center: {
          alignment: 'center'
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        }
      }
    };

    pdfmake.createPdf(hemograma).open();
  }