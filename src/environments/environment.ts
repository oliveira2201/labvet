// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCnDDvQANrU56X_qczuHv2l_kYq8VRD3U4',
    authDomain: 'labadmin-d7418.firebaseapp.com',
    databaseURL: 'https://labadmin-d7418.firebaseio.com',
    projectId: 'labadmin-d7418',
    storageBucket: 'labadmin-d7418.appspot.com',
    messagingSenderId: '845592086615',
    appId: '1:845592086615:web:ba10bee2a8cd858aecbb12',
    measurementId: 'G-H4VSB7WRQM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
