import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-configuracoes-lista',
  templateUrl: './configuracoes-lista.page.html',
  styleUrls: ['./configuracoes-lista.page.scss']
})
export class ConfiguracoesListaPage {
  constructor(private navController: NavController) {}

  configuracoes = [
    {
      nome: 'Exames',
      link: '/exames'
    },
    {
      nome: 'Grupos',
      link: '/grupos'
    }
  ];
  obterLink($event) {
    this.navController.navigateForward($event.link);
  }
}
