import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfiguracoesListaPage } from './configuracoes-lista.page';
import { ConfiguracoesItemComponent } from '../../components/configuracoes-item/configuracoes-item.component';

const routes: Routes = [
  {
    path: '',
    component: ConfiguracoesListaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfiguracoesListaPage, ConfiguracoesItemComponent]
})
export class ConfiguracoesListaPageModule {}
