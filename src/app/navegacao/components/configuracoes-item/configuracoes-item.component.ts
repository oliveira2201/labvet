import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Configuracao } from '../../models/configuracoes.model';

@Component({
  selector: 'app-configuracoes-item',
  templateUrl: './configuracoes-item.component.html',
  styleUrls: ['./configuracoes-item.component.scss']
})
export class ConfiguracoesItemComponent {
  @Input() configuracao;
  @Output() link = new EventEmitter<Configuracao>();
}
