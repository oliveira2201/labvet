import { Exame } from 'src/app/exames/models/exames.model';

export interface Grupo {
  id: string;
  nome: string;
  exames: [
    {
      id: string;
      nome: string;
      categoria: string;
      ordem: number;
      avulso: boolean;
      valores: [
        {
          unidade: string;
          tipo: string;
          formula: string;
          referencias: [
            {
              espécie: string;
              limiteInferior: number;
              limiteSuperior: number;
            }
          ];
        }
      ];
    }
  ];
}
