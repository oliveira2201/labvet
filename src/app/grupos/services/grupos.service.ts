import { Injectable } from '@angular/core';
import { Repository } from 'src/app/core/classes/repository.class';
import { Grupo } from '../models/grupos.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListasService } from 'src/app/listas/services/listas.service';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GruposService extends Repository<Grupo> {
  constructor(db: AngularFirestore, private listasService: ListasService) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/grupos');
  }

  atualizar(item: Grupo): Promise<Grupo> {
    this.atualizarListaExames(item);
    return super.atualizar(item);
  }

  criar(item: Grupo, id: string): Promise<Grupo> {
    this.atualizarListaExames(item);
    return super.criar(item, id);
  }

  private atualizarListaExames(exame) {
    let exameTemp = {
      nome: exame.nome,
      tipo: 'grupo'
    };

    this.listasService
      .obterPorId('exames')
      .pipe(first())
      .subscribe(({ lista }) => {
        let existeExame = false;
        // Verifica se o exama já existe na lista
        for (const exame of lista) {
          // Se existir fazer nada
          if (exameTemp.nome === exame['nome']) {
            existeExame = true;
            return;
          }
        }
        // Adicionar na lista caso n exista ainda
        if (!existeExame) {
          lista.push(exameTemp);
          this.listasService.atualizar({
            id: 'exames',
            lista
          });
        }
      });

    // this.listasService
    //   .obterPorId('exames')
    //   .pipe(first())
    //   .subscribe(({ lista }) => {
    //     if (
    //       !lista.find(nome => {
    //         return nome === grupo.nome;
    //       })
    //     ) {
    //       lista.push(grupo.nome);
    //       this.listasService.atualizar({
    //         id: 'examesDisponiveis',
    //         lista
    //       });
    //     }
    //   });
  }
}
