import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'criar',
        loadChildren:
          './pages/grupos-salvar/grupos-salvar.module#GruposSalvarPageModule'
      },
      {
        path: 'editar/:id',
        loadChildren:
          './pages/grupos-salvar/grupos-salvar.module#GruposSalvarPageModule'
      },
      {
        path: '',
        loadChildren:
          './pages/grupos-lista/grupos-lista.module#GruposListaPageModule'
      }
    ]
  },
  {
    path: 'grupos-salvar',
    loadChildren:
      './pages/grupos-salvar/grupos-salvar.module#GruposSalvarPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GruposRoutingModule {}
