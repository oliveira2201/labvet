import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Grupo } from '../../models/grupos.model';

@Component({
  selector: 'app-grupos-item',
  templateUrl: './grupos-item.component.html',
  styleUrls: ['./grupos-item.component.scss']
})
export class GruposItemComponent {
  @Input() grupo: Grupo;
  @Output() atualizado = new EventEmitter<Grupo>();
  @Output() deletado = new EventEmitter<Grupo>();
}
