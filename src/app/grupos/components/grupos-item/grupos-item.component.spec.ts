import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposItemComponent } from './grupos-item.component';

describe('GruposItemComponent', () => {
  let component: GruposItemComponent;
  let fixture: ComponentFixture<GruposItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposItemComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
