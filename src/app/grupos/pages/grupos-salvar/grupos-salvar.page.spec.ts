import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposSalvarPage } from './grupos-salvar.page';

describe('GruposSalvarPage', () => {
  let component: GruposSalvarPage;
  let fixture: ComponentFixture<GruposSalvarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposSalvarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposSalvarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
