import { Component, OnInit } from '@angular/core';
import { GruposService } from '../../services/grupos.service';
import { ListasService } from 'src/app/listas/services/listas.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { FormBuilder } from '@angular/forms';
import { first, take } from 'rxjs/operators';
import { ExamesService } from 'src/app/exames/services/exames.service';
import { Exame } from 'src/app/exames/models/exames.model';

@Component({
  selector: 'app-grupos-salvar',
  templateUrl: './grupos-salvar.page.html',
  styleUrls: ['./grupos-salvar.page.scss']
})
export class GruposSalvarPage {
  constructor(
    private fb: FormBuilder,
    private examesService: ExamesService,
    private gruposService: GruposService,
    private listasService: ListasService,
    private route: ActivatedRoute,
    private navController: NavController,
    private overlayService: OverlayService
  ) {}
  examesCarregados = false;
  titulo = '';
  grupoId: string = undefined;
  examesDisponiveis = [];
  gruposForm = this.fb.group({
    id: '',
    nome: '',
    examesEscolhidos: ['']
  });

  ngOnInit() {
    const grupoId = this.route.snapshot.paramMap.get('id');

    if (!grupoId) {
      this.titulo = 'Cadastrar Grupo';
    } else {
      this.titulo = 'Editar Grupo';
      // Obter dados do grupo selecionado
      this.gruposService
        .obterPorId(grupoId)
        .pipe(first())
        .subscribe(dados => {
          this.gruposForm.patchValue(dados);
        });
    }
    this.grupoId = grupoId;

    // Obter os dados da lista
    this.listasService
      .obterPorId('exames')
      .pipe(first())
      .subscribe(({ lista }) => {
        lista.forEach(exame => {
          if (exame.tipo === 'exame') {
            this.examesDisponiveis.push(exame.nome);
            this.examesCarregados = true;
          }
        });
      });
  }

  obterDadosExame(id: string) {
    return this.examesService
      .obterPorId(id)
      .pipe(take(1))
      .toPromise()
      .then(resultado => {
        return resultado;
      });
  }

  async onSubmit(): Promise<void> {
    try {
      const examesPromises = [];
      const grupoEntrada = this.gruposForm.value;
      grupoEntrada.examesEscolhidos.forEach(async exame => {
        examesPromises.push(this.obterDadosExame(exame));
      });
      const examesSaida = await Promise.all(examesPromises).then(exames => {
        return (grupoEntrada.exames = exames);
      });
      grupoEntrada.exames = examesSaida;
      this.gruposService
        .criar(grupoEntrada, grupoEntrada.nome)
        .then(grupoCriado => {});
    } catch (e) {
      console.log(e);
    }
  }
  isDadosCarregados(): boolean {
    return this.examesDisponiveis && this.examesCarregados;
  }
}
