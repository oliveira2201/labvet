import { Component, OnInit } from '@angular/core';
import { Grupo } from '../../models/grupos.model';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { GruposService } from '../../services/grupos.service';

@Component({
  selector: 'app-grupos-lista',
  templateUrl: './grupos-lista.page.html',
  styleUrls: ['./grupos-lista.page.scss']
})
export class GruposListaPage {
  grupos$: Observable<Grupo[]>;

  constructor(
    private navControler: NavController,
    private gruposService: GruposService
  ) {}

  ionViewDidEnter(): void {
    this.grupos$ = this.gruposService.obterTodos();
  }

  onUpdate(grupo: Grupo): void {
    this.navControler.navigateForward(`/grupos/editar/${grupo.id}`);
  }
}
