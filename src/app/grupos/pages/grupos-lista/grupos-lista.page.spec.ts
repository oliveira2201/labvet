import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposListaPage } from './grupos-lista.page';

describe('GruposListaPage', () => {
  let component: GruposListaPage;
  let fixture: ComponentFixture<GruposListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposListaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
