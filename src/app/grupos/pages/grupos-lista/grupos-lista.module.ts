import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GruposListaPage } from './grupos-lista.page';
import { GruposItemComponent } from '../../components/grupos-item/grupos-item.component';

const routes: Routes = [
  {
    path: '',
    component: GruposListaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GruposListaPage, GruposItemComponent]
})
export class GruposListaPageModule {}
