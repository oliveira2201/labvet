export interface Requisicao {
  id: string;
  nomeAnimal: string;
  especie: string;
  raça: string;
  sexo: string;
  idade: number;
  dataEntrada: Date;
  dataSaida: Date;
  tutor: string;
  veterinarioSolicitante: string;
  clinicaSolicitante: string;
  finalizado: boolean;
  examesCadastrados: [string];
  grupos: [
    {
      id: string;
      nome: string;
      exames: {
        nome: string;
        valores: [
          {
            unidade: string;
            formula: string;
            tipo: string;
            limiteSuperior: number;
            limiteInferior: number;
            resultado: string;
          }
        ];
      };
    }
  ];
}
