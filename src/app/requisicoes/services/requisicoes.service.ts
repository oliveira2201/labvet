import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Requisicao } from '../models/requisicaomodel';
import { Repository } from 'src/app/core/classes/repository.class';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class RequisicoesService extends Repository<Requisicao> {
  constructor(db: AngularFirestore) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/requisicoes', (ref: firestore.CollectionReference) => {
      return ref.orderBy('dataEntrada', 'desc');
    });
  }
}
