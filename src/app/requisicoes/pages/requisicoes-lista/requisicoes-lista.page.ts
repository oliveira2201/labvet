import { Component, OnInit } from '@angular/core';
import { RequisicoesService } from '../../services/requisicoes.service';
import { Observable } from 'rxjs';
import { Requisicao } from '../../models/requisicaomodel';
import { NavController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';

@Component({
  selector: 'app-requisicoes-lista',
  templateUrl: './requisicoes-lista.page.html',
  styleUrls: ['./requisicoes-lista.page.scss']
})
export class RequisicoesListaPage implements OnInit {
  requisicoes$: Observable<Requisicao[]>;
  constructor(
    private requisicoesService: RequisicoesService,
    private navController: NavController,
    private overlayService: OverlayService
  ) {}

  ngOnInit() {
    this.requisicoes$ = this.requisicoesService.obterTodos();
  }

  onUpdate(requisicao: Requisicao): void {
    this.navController.navigateForward(`/requisicoes/editar/${requisicao.id}`);
  }

  async onDelete(requisicao: Requisicao): Promise<void> {
    console.log('delete');
    await this.overlayService.alert({
      message: `Deseja realmente apagar a requisição do ${requisicao.nomeAnimal}?`,
      buttons: [
        {
          text: 'Yes',
          handler: async () => {
            await this.requisicoesService.delete(requisicao);
          }
        },
        'Não'
      ]
    });
  }
}
