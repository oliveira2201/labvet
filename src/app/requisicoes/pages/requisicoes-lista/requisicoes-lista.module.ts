import { NgModule, Input, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequisicoesListaPage } from './requisicoes-lista.page';
import { RequisicoesItemComponent } from '../../components/requisicoes-item/requisicoes-item.component';
import { Requisicao } from '../../models/requisicaomodel';

const routes: Routes = [
  {
    path: '',
    component: RequisicoesListaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RequisicoesListaPage, RequisicoesItemComponent]
})
export class RequisicoesListaPageModule {}
