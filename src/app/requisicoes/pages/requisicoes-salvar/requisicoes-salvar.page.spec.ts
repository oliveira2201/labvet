import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisicoesSalvarPage } from './requisicoes-salvar.page';

describe('RequisicoesSalvarPage', () => {
  let component: RequisicoesSalvarPage;
  let fixture: ComponentFixture<RequisicoesSalvarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisicoesSalvarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisicoesSalvarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
