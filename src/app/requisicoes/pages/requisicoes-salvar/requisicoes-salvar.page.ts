import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { RequisicoesService } from '../../services/requisicoes.service';
import { take, first, skip, pairwise, map } from 'rxjs/operators';
import { ListasService } from 'src/app/listas/services/listas.service';
import { ExamesService } from 'src/app/exames/services/exames.service';
import { GruposService } from 'src/app/grupos/services/grupos.service';
import { RelatoriosService } from '../../services/relatorios.service';
import { ZoomFormComponent } from 'src/app/shared/components/zoom-form/zoom-form.component';
import { Observable, combineLatest, forkJoin } from 'rxjs';
import { Lista } from 'src/app/listas/models/listas.model';

@Component({
  selector: 'app-requisicoes-salvar',
  templateUrl: './requisicoes-salvar.page.html',
  styleUrls: ['./requisicoes-salvar.page.scss']
})
export class RequisicoesSalvarPage {
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private navController: NavController,
    private overlayService: OverlayService,
    private requisicoesService: RequisicoesService,
    private listasService: ListasService,
    private examesService: ExamesService,
    private gruposService: GruposService,
    private relatoriosService: RelatoriosService,
    public modalController: ModalController
  ) {}
  listaEspecies$: Observable<Lista>;
  listaExames$: Observable<Lista>;
  listas$;
  tabAtual = 'dados';
  dadosCarregados = false;
  requisicaoId: string = undefined;
  titulo = '';
  erro = '';
  exameSelecionado = '';
  listaExameObj = [];
  listaEspecies = [];
  listaEspeciesCarregada = false;
  grupos = [];

  get grupo() {
    if (this.grupos && this.exameSelecionado) {
      for (const exame of this.grupos) {
        if (exame.nome === this.exameSelecionado) {
          return exame;
        }
      }
    }
    return null;
  }
  examesListaCarregada = false;
  examesDisponiveis = [];
  requisicoesForm = this.fb.group({
    nomeAnimal: '',
    especie: '',
    raça: '',
    sexo: '',
    idade: '',
    dataEntrada: '',
    dataSaida: '',
    tutor: '',
    veterinarioSolicitante: '',
    clinicaSolicitante: '',
    examesCadastrados: '',
    grupos: ['']
  });

  async abrirModal(campo, exame) {
    console.log(campo);
    console.log(exame);

    const modal = await this.modalController.create({
      component: ZoomFormComponent,
      componentProps: {
        valor: campo.resultado,
        titulo:
          campo.nome !== 'a' && campo.nome !== 'b' ? campo.nome : exame.nome
      }
    });

    modal.onDidDismiss().then(data => {
      if (data.data !== undefined) {
        campo.resultado = data.data;
      }
    });
    return await modal.present();
  }

  teste(ev) {
    console.log('this.requisicoesForm.value');
    console.log(this.requisicoesForm.value);
    console.log('this.grupos');
    console.log(this.grupos);
  }

  calcularCampos() {
    if (this.customFilter(this.requisicoesForm.value, 'nome', 'Eritrograma')) {
      const hematocrito = this.customFilter(
        this.requisicoesForm.value,
        'nome',
        'Hematócrito'
      );
      const hematocritoResultado = parseFloat(
        this.customFilter(hematocrito, 'nome', 'a').resultado.replace(',', '.')
      );

      const hemacias = this.customFilter(
        this.requisicoesForm.value,
        'nome',
        'Hemácias'
      );
      const hemaciasResultado = parseFloat(
        this.customFilter(hemacias, 'nome', 'a').resultado.replace(',', '.')
      );

      const hemoglobina = this.customFilter(
        this.requisicoesForm.value,
        'nome',
        'Hemácias'
      );
      const hemoglobinaResultado = parseFloat(
        this.customFilter(hemacias, 'nome', 'a').resultado.replace(',', '.')
      );

      const VCM = this.customFilter(this.requisicoesForm.value, 'nome', 'VCM');
      this.customFilter(VCM, 'nome', 'a').resultado = (
        (hematocritoResultado * 10) /
        hemaciasResultado
      ).toFixed(1);

      const CHCM = this.customFilter(
        this.requisicoesForm.value,
        'nome',
        'CHCM'
      );
      this.customFilter(CHCM, 'nome', 'a').resultado = (
        (hemoglobinaResultado * 100) /
        hematocritoResultado
      ).toFixed(1);
    }

    if (this.customFilter(this.requisicoesForm.value, 'nome', 'Leucograma')) {
      const leucocitos = this.customFilter(
        this.requisicoesForm.value,
        'nome',
        'Leucócitos'
      );
      const leucocitosResultado = parseFloat(
        this.customFilter(leucocitos, 'nome', 'b').resultado.replace(',', '.')
      );

      let campos = [
        'Mielócitos',
        'Metamielócitos',
        'Bastonetes',
        'Segmentados',
        'Eosinófilos',
        'Basófilos',
        'Linfócitos',
        'Monócitos'
      ];
      campos.forEach(campo => {
        const a = this.customFilter(this.requisicoesForm.value, 'nome', campo);
        const resultado0 = parseFloat(
          this.customFilter(a, 'nome', 'a').resultado.replace(',', '.')
        );
        this.customFilter(a, 'nome', 'b').resultado = (
          (leucocitosResultado * resultado0) /
          100
        ).toFixed(0);
      });
    }
  }

  async salvarDados() {
    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      this.requisicoesForm.value.grupos = this.grupos;
      this.calcularCampos();
      const exame = !this.requisicaoId
        ? await this.requisicoesService
            .criar(this.requisicoesForm.value)
            .then(exameCriado => {
              return exameCriado;
            })
        : await this.requisicoesService
            .atualizar({
              id: this.requisicaoId,
              ...this.requisicoesForm.value
            })
            .then(exameAtualizado => {
              return exameAtualizado;
            });
      return exame;
    } catch (e) {
      console.error(e);
      this.erro = e;
    } finally {
      loading.dismiss();
    }
  }

  async onSubmit() {
    const requisicao = await this.salvarDados();
    this.navController.navigateForward(`/requisicoes/editar/${requisicao.id}`);
  }

  async examesChange(ev: any) {
    const loading = await this.overlayService.loading({
      message: 'Atualizando lista de exames...'
    });
    try {
      let gruposTemp = [];
      for (let exame of this.requisicoesForm.value.examesCadastrados) {
        let grupoExiste = false;
        if (this.grupos) {
          // Verifica se o exame já está cadastrado
          for (const grupo of this.grupos) {
            if (grupo.nome === exame) {
              console.log(grupo.nome, ' já cadastrado');
              gruposTemp.push(grupo);
              grupoExiste = true;
              break;
            }
          }
          // Depois de percorrer todos grupo, não existir o item
          // Obter do banco de dados
          if (!grupoExiste) {
            console.log(exame, ' não cadastrado');
            const exameObtido = await this.obterExames(exame);
            const exameModificado = await this.modificarGrupo(exameObtido);
            gruposTemp.push(exameModificado);
          }
        }
      }
      this.grupos = JSON.parse(JSON.stringify(gruposTemp));
      // const requisicao = await this.salvarDados();
      // this.navController.navigateForward(
      //   `/requisicoes/editar/${requisicao.id}`
      // );
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
    }
  }

  customFilter(object, prop, value) {
    if (object.hasOwnProperty(prop) && object[prop] == value) return object;

    for (var i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == 'object') {
        var o = this.customFilter(object[Object.keys(object)[i]], prop, value);
        if (o != null) return o;
      }
    }

    return null;
  }

  resultadoChange(e) {
    // let a = this.customFilter(this.requisicoesForm.value);
    // a.VCM['0'].resultado = a.Hematócrito['0'].resultado * 10;
  }

  private async modificarGrupo(temp) {
    console.log('modificando ', temp.nome);
    temp.idadeMaxima = 240;
    temp.idadeMinima = 0;
    let distanciaAntiga = 240;

    let grupoTemp = {
      exames: [],
      nome: ''
    };
    // Caso não seja um grupo, cria um grupo com o nome do exame e adiciona ela a array exames
    // Feito para deixar no mesmo formado que um grupo
    if (!temp.hasOwnProperty(`exames`)) {
      grupoTemp.nome = temp.nome;
      grupoTemp.exames.push(temp);
      temp = grupoTemp;
    }
    // Ordena conforme o campo ordem
    temp.exames = temp.exames.sort(function(a, b) {
      var x = a.ordem;
      var y = b.ordem;
      return x < y ? -1 : x > y ? 1 : 0;
    });
    temp.categoria = [];
    // Para cada exame cadastrado
    temp.exames.forEach(exame => {
      // A propriedade avulso removida
      // Essa propriedade é somente necessária para lista de exames disponíveis
      delete exame.avulso;
      // Para cada campo do exame
      exame.campos.forEach(campo => {
        // Obtem o limiteInferior e limiteSuperior de cada campo, com base na espécie e idade
        const referencias = this.obterReferencias(campo);
        temp.especie = referencias.especie;
        if (campo.valorPadrao) {
          campo.resultado = campo.valorPadrao;
        }
        if (referencias) {
          campo.limiteInferior = referencias.limiteInferior;
          campo.limiteSuperior = referencias.limiteSuperior;
          // Ver qual faixa mais restrita a referencia se encaixa
          let distanciaNova =
            parseInt(referencias.idadeMaxima, 10) -
            parseInt(referencias.idadeMinima, 10);

          if (distanciaNova < distanciaAntiga) {
            distanciaNova = distanciaAntiga;
            temp.idadeMinima = referencias.idadeMinima;
            temp.idadeMaxima = referencias.idadeMaxima;
          }
        }

        // A propriedade especies é removida
        // Essa propriedade armazena todos referências, incluindo as não necessárias
        delete campo.especies;
      });
      // Obtém a referência da categoria do grupo, caso exista
      const cat = temp.categoria.find(obj => obj.nome === exame.categoria);
      // Se a categoria existe, coloca o exame dentro da categoria
      if (cat) {
        cat.exames.push(exame);
        // Caso a categoria não exista
      } else {
        let categoriaTemp = {
          nome: exame.categoria,
          exames: []
        };
        // É criado uma nova e coloca o exame dentro da categoria
        categoriaTemp.exames.push(exame);
        temp.categoria.push(categoriaTemp);
      }
    });
    // A propriedade exames é removida
    // Pois os exames já estão dentro da respectiva categoria
    delete temp.exames;

    return temp;
  }

  private obterReferencias(campo) {
    let referencias = {
      limiteInferior: '',
      limiteSuperior: '',
      idadeMinima: '',
      idadeMaxima: '',
      especie: ''
    };
    const idadeAnimal = parseInt(this.requisicoesForm.value.idade, 10);
    for (const especie of campo.especies) {
      if (especie.especie === this.requisicoesForm.value.especie) {
        console.log(especie.especie);
        for (const idade of especie.idades) {
          const idadeMinima = parseInt(idade.idadeMinima, 10);
          const idadeMaxima = parseInt(idade.idadeMaxima, 10);
          if (idadeAnimal >= idadeMinima && idadeAnimal < idadeMaxima) {
            referencias.limiteInferior = idade.limiteInferior;
            referencias.limiteSuperior = idade.limiteSuperior;
            referencias.idadeMinima = idade.idadeMinima;
            referencias.idadeMaxima = idade.idadeMaxima;
            referencias.especie = especie.especie;
            break;
          }
        }
      }
    }
    return referencias;
  }

  private async obterExames(nome) {
    let tipoExame = '';
    let id = '';
    let resultado = {};
    // Obter o tipo de exame na lista de exames
    for (const exame of this.listaExameObj) {
      if (nome === exame.nome) {
        console.log('exame');
        console.log(exame);
        tipoExame = exame.tipo;
        id = exame.id;
        break;
      }
    }
    console.log('id');
    console.log(id);
    if (tipoExame === 'exame') {
      resultado = await this.examesService
        .obterPorId(id)
        .pipe(take(1))
        .toPromise()
        .then(x => {
          console.log('x');
          console.log(x);
          return x;
        });
    } else {
      resultado = await this.gruposService
        .obterPorId(nome)
        .pipe(take(1))
        .toPromise()
        .then(x => {
          return x;
        });
    }
    // Propriedade necessária somente no cadastro de grupos
    delete resultado['examesEscolhidos'];
    return resultado;
  }

  private filtrarGruposEExamesAvulsos(exame) {
    if (exame.tipo === 'grupo') {
      return true;
    } else {
      if (exame.avulso) {
        return true;
      }
    }
    return false;
  }

  ionViewDidEnter() {
    this.listaEspecies$ = this.listasService.obterPorId('espécies');
    this.listaExames$ = this.listasService.obterPorId('exames');
    this.listas$ = combineLatest([this.listaEspecies$, this.listaExames$]).pipe(
      map(([especies, exames]) => {
        return {
          especies: especies.lista,
          exames: exames.lista.filter(this.filtrarGruposEExamesAvulsos)
        };
      })
    );

    const requisicaoId = this.route.snapshot.paramMap.get('id');
    // Se tem id na URL, carrega a requisição
    if (requisicaoId) {
      this.requisicaoId = requisicaoId;
      this.titulo = 'Editar Requisição';
      // Obter dados da requisição selecionado
      this.requisicoesService
        .obterPorId(requisicaoId)
        .pipe(take(1))
        .subscribe(dados => {
          this.requisicoesForm.patchValue(dados);
          this.grupos = this.requisicoesForm.value.grupos;
          this.dadosCarregados = true;
          console.log(dados);
        });
      // Caso não tenha id, muda para modo cadastro
    } else {
      this.titulo = 'Cadastrar Requisição';
      this.dadosCarregados = true;
      return;
    }
  }

  // ionViewWillLeave() {
  //   this.salvarDados();
  // }

  carregarRequisições() {}

  isDadosCarregados(): boolean {
    return (
      this.dadosCarregados &&
      this.examesListaCarregada &&
      this.listaEspeciesCarregada
    );
  }

  async gerarPDF() {
    await this.salvarDados();
    this.requisicoesForm.value.grupos = this.grupos;
    let dados = JSON.parse(JSON.stringify(this.requisicoesForm.value));
    this.relatoriosService.gerarPDF(dados);
  }
}
