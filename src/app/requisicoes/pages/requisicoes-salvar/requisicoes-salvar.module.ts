import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequisicoesSalvarPage } from './requisicoes-salvar.page';
import { ZoomFormComponent } from 'src/app/shared/components/zoom-form/zoom-form.component';

const routes: Routes = [
  {
    path: '',
    component: RequisicoesSalvarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RequisicoesSalvarPage, ZoomFormComponent],
  entryComponents: [ZoomFormComponent]
})
export class RequisicoesSalvarPageModule {}
