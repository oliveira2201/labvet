import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequisicoesRoutingModule } from './requisicoes-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RequisicoesRoutingModule
  ]
})
export class RequisicoesModule { }
