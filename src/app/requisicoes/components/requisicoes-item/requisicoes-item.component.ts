import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Requisicao } from '../../models/requisicaomodel';

@Component({
  selector: 'app-requisicoes-item',
  templateUrl: './requisicoes-item.component.html',
  styleUrls: ['./requisicoes-item.component.scss']
})
export class RequisicoesItemComponent implements OnInit {
  @Input() requisicao: Requisicao;
  @Output() atualizar = new EventEmitter<Requisicao>();
  @Output() excluir = new EventEmitter<Requisicao>();

  constructor() {}

  ngOnInit() {}
}
