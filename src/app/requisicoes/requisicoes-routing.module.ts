import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren:
          './pages/requisicoes-lista/requisicoes-lista.module#RequisicoesListaPageModule'
      },
      {
        path: 'criar',
        loadChildren:
          './pages/requisicoes-salvar/requisicoes-salvar.module#RequisicoesSalvarPageModule'
      },
      {
        path: 'editar/:id',
        loadChildren:
          './pages/requisicoes-salvar/requisicoes-salvar.module#RequisicoesSalvarPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequisicoesRoutingModule {}
