import {
  AngularFirestore,
  AngularFirestoreCollection,
  QueryFn
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export abstract class Repository<T extends { id: string }> {
  protected collection: AngularFirestoreCollection<T>;
  constructor(protected db: AngularFirestore) {}

  protected setCollection(path: string, queryFn?: QueryFn): void {
    this.collection = path ? this.db.collection(path, queryFn) : null;
  }

  obterTodos(): Observable<T[]> {
    return this.collection.valueChanges();
  }

  obterPorId(id: string): Observable<T> {
    return this.collection.doc<T>(id).valueChanges();
  }

  protected setItem(item: T, operacao: 'set' | 'update'): Promise<T> {
    return this.collection
      .doc<T>(item.id)
      [operacao](item)
      .then(() => item);
  }

  criar(item: T, id: string = undefined): Promise<T> {
    id === undefined ? (item.id = this.db.createId()) : (item.id = id);
    return this.setItem(item, 'set');
  }

  atualizar(item: T): Promise<T> {
    return this.setItem(item, 'update');
  }

  delete(item: T): Promise<void> {
    return this.collection.doc<T>(item.id).delete();
  }
}
