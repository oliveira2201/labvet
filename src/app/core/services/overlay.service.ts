import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { LoadingOptions, AlertOptions } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {
  constructor(
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  async loading(options?: LoadingOptions): Promise<HTMLIonLoadingElement> {
    const loading = await this.loadingController.create({
      message: 'Carregando...',
      ...options
    });
    await loading.present();
    return loading;
  }

  async alert(options?: AlertOptions): Promise<HTMLIonAlertElement> {
    const alert = await this.alertController.create(options);
    await alert.present();
    return alert;
  }
}
