export interface Exame {
  b: any;
  id: string;
  nome: string;
  categoria: string;
  ordem: number;
  avulso: boolean;
  campos: [
    {
      unidade: string;
      tipo: string;
      formula: string;
      resultado: string;
      especies: [
        {
          especie: string;
          idades: [
            {
              idadeMinima: number;
              idadeMaxima: number;
              limiteInferior: number;
              limiteSuperior: number;
            }
          ];
        }
      ];
    }
  ];
}
