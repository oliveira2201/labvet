import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamesListaPage } from './exames-lista.page';

describe('ExamesListaPage', () => {
  let component: ExamesListaPage;
  let fixture: ComponentFixture<ExamesListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamesListaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamesListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
