import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExamesListaPage } from './exames-lista.page';
import { ExamesItemComponent } from '../../components/exames-item/exames-item.component';

const routes: Routes = [
  {
    path: '',
    component: ExamesListaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExamesListaPage, ExamesItemComponent]
})
export class ExamesListaPageModule {}
