import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Exame } from '../../models/exames.model';
import { ExamesService } from '../../services/exames.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-exames-lista',
  templateUrl: './exames-lista.page.html',
  styleUrls: ['./exames-lista.page.scss']
})
export class ExamesListaPage {
  exames$: Observable<Exame[]>;

  constructor(
    private navControler: NavController,
    private examesService: ExamesService
  ) {}

  ionViewDidEnter(): void {
    this.exames$ = this.examesService.obterTodos();
  }

  onUpdate(exame: Exame): void {
    this.navControler.navigateForward(`/exames/editar/${exame.id}`);
  }
}
