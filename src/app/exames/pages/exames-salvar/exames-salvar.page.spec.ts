import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamesSalvarPage } from './exames-salvar.page';

describe('ExamesSalvarPage', () => {
  let component: ExamesSalvarPage;
  let fixture: ComponentFixture<ExamesSalvarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamesSalvarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamesSalvarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
