import { Component, OnInit, ɵConsole, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { ExamesService } from '../../services/exames.service';
import { ActivatedRoute } from '@angular/router';
import { take, first, map } from 'rxjs/operators';
import { ListasService } from 'src/app/listas/services/listas.service';
import { Lista } from 'src/app/listas/models/listas.model';
import { NavController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { Exame } from '../../models/exames.model';
import { GruposService } from 'src/app/grupos/services/grupos.service';

@Component({
  selector: 'app-exames-salvar',
  templateUrl: './exames-salvar.page.html',
  styleUrls: ['./exames-salvar.page.scss']
})
export class ExamesSalvarPage {
  constructor(
    private fb: FormBuilder,
    private examesService: ExamesService,
    private listasService: ListasService,
    private gruposService: GruposService,
    private route: ActivatedRoute,
    private navController: NavController,
    private overlayService: OverlayService
  ) {}
  erro: string = null;
  grupos = [];
  titulo = '';
  exameId: string = undefined;
  categorias: string[];
  tiposDeValor: string[];
  especies: string[];
  examesCarregados = false;

  examesForm = this.fb.group({
    nome: '',
    categoria: '',
    ordem: '',
    avulso: '',
    campos: this.fb.array([])
  });
  // ******************* Campos ************************** //
  inicializarCampoForm() {
    return this.fb.group({
      nome: '',
      unidade: '',
      tipo: '',
      formula: '',
      valorPadrao: '',
      especies: this.fb.array([])
    });
  }

  adicionarCampoForm() {
    const control = (<FormArray>(
      this.examesForm.controls['campos']
    )) as FormArray;
    control.push(this.inicializarCampoForm());
  }

  // ******************* Espécies ************************** //
  inicializarEspecieForm() {
    return this.fb.group({
      especie: '',
      idades: this.fb.array([])
    });
  }

  adicionarEspecieForm(ix) {
    const control = (<FormArray>this.examesForm.controls['campos'])
      .at(ix)
      .get('especies') as FormArray;
    control.push(this.inicializarEspecieForm());
  }
  // ******************* Idades ************************** //
  inicializarIdadeForm() {
    return this.fb.group({
      idadeMinima: '',
      idadeMaxima: '',
      limiteInferior: '',
      limiteSuperior: ''
    });
  }

  adicionarIdadeForm(ix, iy) {
    console.log(ix, ' ', iy);
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const control = ((<FormArray>this.examesForm.controls['campos'])
      .at(ix)
      .get('especies') as FormArray)
      .at(iy)
      .get('idades') as FormArray;
    control.push(this.inicializarIdadeForm());
  }

  customFilter(object, prop, value) {
    if (object.hasOwnProperty(prop) && object[prop] == value) return object;

    for (var i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == 'object') {
        var o = this.customFilter(object[Object.keys(object)[i]], prop, value);
        if (o != null) return o;
      }
    }

    return null;
  }

  async onSubmit(): Promise<void> {
    // this.atualizarGrupos();

    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      const exame = !this.exameId
        ? await this.examesService
            .criar(
              this.examesForm.value,
              this.criarSlug(this.examesForm.value.nome)
            )
            .then(exameCriado => {
              // this.atualizarRelacoes(exameCriado);
            })
        : await this.examesService
            .atualizar({
              id: this.exameId,
              ...this.examesForm.value
            })
            .then(exameAtualizado => {
              // this.atualizarRelacoes(exameAtualizado);
            });
      this.navController.navigateBack('/exames');
    } catch (e) {
      console.error(e);
      this.erro = e;
    } finally {
      loading.dismiss();
    }
  }

  isDadosCarregados(): boolean {
    return (
      !!this.categorias &&
      !!this.tiposDeValor &&
      !!this.especies &&
      this.examesCarregados
    );
  }

  ngOnInit() {
    const exameId = this.route.snapshot.paramMap.get('id');

    // Obter os dados da lista
    this.listasService
      .obterPorId('categorias')
      .pipe(first())
      .subscribe(({ lista }) => {
        this.categorias = lista;
      });

    this.listasService
      .obterPorId('tiposDeValor')
      .pipe(first())
      .subscribe(({ lista }) => {
        this.tiposDeValor = lista;
      });

    this.listasService
      .obterPorId('espécies')
      .pipe(first())
      .subscribe(({ lista }) => {
        this.especies = lista;
      });

    if (!exameId) {
      this.titulo = 'Cadastrar Exame';
      // this.adicionarValor();
      this.examesCarregados = true;
      return;
    }
    this.exameId = exameId;
    this.titulo = 'Editar Exame';
    // Obter dados do exame selecionado
    this.examesService
      .obterPorId(exameId)
      .pipe(take(1))
      .subscribe(dados => {
        dados.campos.forEach((campo, x) => {
          this.adicionarCampoForm();
          campo.especies.forEach((especie, y) => {
            this.adicionarEspecieForm(x);
            especie.idades.forEach(idade => {
              this.adicionarIdadeForm(x, y);
            });
          });
        });
        this.examesForm.patchValue(dados);
      });
    this.examesCarregados = true;
  }

  criarSlug(str) {
    return str
      .toLowerCase()
      .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
      .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
      .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
      .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
      .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
      .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
      .replace(/[ñÑ]+/g, 'n') // Special Characters #7
      .replace(/[çÇ]+/g, 'c') // Special Characters #8
      .replace(/[ß]+/g, 'ss') // Special Characters #9
      .replace(/[Ææ]+/g, 'ae') // Special Characters #10
      .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
      .replace(/[%]+/g, 'pct') // Special Characters #12
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }
}
