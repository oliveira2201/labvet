import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'criar',
        loadChildren:
          './pages/exames-salvar/exames-salvar.module#ExamesSalvarPageModule'
      },
      {
        path: 'editar/:id',
        loadChildren:
          './pages/exames-salvar/exames-salvar.module#ExamesSalvarPageModule'
      },
      {
        path: '',
        loadChildren:
          './pages/exames-lista/exames-lista.module#ExamesListaPageModule'
      }
    ]
  }
  // {
  //   path: 'exames-salvar',
  //   loadChildren:
  //     './pages/exames-salvar/exames-salvar.module#ExamesSalvarPageModule'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamesRoutingModule {}
