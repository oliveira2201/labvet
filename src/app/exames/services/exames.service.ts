import { Injectable } from '@angular/core';
import { Repository } from 'src/app/core/classes/repository.class';
import { Exame } from '../models/exames.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListasService } from 'src/app/listas/services/listas.service';
import { GruposService } from 'src/app/grupos/services/grupos.service';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamesService extends Repository<Exame> {
  constructor(
    db: AngularFirestore,
    private listasService: ListasService,
    private gruposService: GruposService
  ) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/exames');
  }

  atualizar(item: Exame): Promise<Exame> {
    this.atualizarRelacoes(item);
    return super.atualizar(item);
  }

  criar(item: Exame, id: string): Promise<Exame> {
    this.atualizarRelacoes(item);
    return super.criar(item, id);
  }

  private atualizarRelacoes(exame: Exame) {
    this.atualizarListaExames(exame);
    this.atualizarGrupos(exame);
  }

  private atualizarListaExames(exame) {
    let exameTemp = {
      nome: exame.nome,
      id: exame.id,
      tipo: 'exame',
      avulso: exame.avulso
    };

    this.listasService
      .obterPorId('exames')
      .pipe(first())
      .subscribe(({ lista }) => {
        let existeExame = false;
        let atualizarExame = false;
        // Verifica se o exame já existe na lista
        for (const exame of lista) {
          if (exameTemp.nome === exame['nome']) {
            existeExame = true;
            // Se existir e as propriedades foram iguais, fazer nada
            if (exame['avulso'] === exameTemp.avulso) {
              return;
            } else {
              // Se foram diferentes, atualizar campo
              exame['avulso'] = exameTemp.avulso;
              atualizarExame = true;
            }
            existeExame = true;
            break;
          }
        }
        // Adicionar na lista caso n exista ainda
        if (!existeExame) {
          lista.push(exameTemp);
          this.listasService.atualizar({
            id: 'exames',
            lista
          });
          return;
        }
        // Atualizar com o valor novo, caso modificada
        if (atualizarExame) {
          this.listasService.atualizar({
            id: 'exames',
            lista
          });
        }
      });
  }

  async atualizarGrupos(exame) {
    const grupos$ = await this.gruposService
      .obterTodos()
      .pipe(first())
      .toPromise()
      .then(x => x);

    grupos$.forEach(grupo => {
      let b = this.customFilter(grupo, 'nome', exame.nome);
      if (b) {
        b = Object.assign(b, exame);
        this.gruposService.atualizar({
          id: grupo.id,
          ...grupo
        });
      }
    });
  }

  customFilter(object, prop, value) {
    if (object.hasOwnProperty(prop) && object[prop] == value) return object;

    for (var i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == 'object') {
        var o = this.customFilter(object[Object.keys(object)[i]], prop, value);
        if (o != null) return o;
      }
    }
  }
}
