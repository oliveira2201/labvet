import { NgModule } from '@angular/core';

import { ExamesRoutingModule } from './exames-routing.module';

@NgModule({
  declarations: [],
  imports: [ExamesRoutingModule]
})
export class ExamesModule {}
