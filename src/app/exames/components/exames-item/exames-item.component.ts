import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Exame } from '../../models/exames.model';

@Component({
  selector: 'app-exames-item',
  templateUrl: './exames-item.component.html',
  styleUrls: ['./exames-item.component.scss']
})
export class ExamesItemComponent {
  @Input() exame: Exame;
  @Output() atualizado = new EventEmitter<Exame>();
  @Output() deletado = new EventEmitter<Exame>();
}
