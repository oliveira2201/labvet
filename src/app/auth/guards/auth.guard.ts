import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivate,
  CanLoad,
  UrlSegment,
  Route,
  CanActivateChild
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {
  constructor(private authService: AuthService, private router: Router) {}

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    return this.checarSeAutenticado(state.url);
  }

  private async checarSeAutenticado(redirect: string): Promise<boolean> {
    if (await this.authService.estaAutenticado) {
      return true;
    } else {
      this.router.navigate(['/login'], {
        queryParams: { redirect }
      });
      return false;
    }
  }
}
