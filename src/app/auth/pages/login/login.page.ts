import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { NavController } from '@ionic/angular';
import { Routes, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  loginForm = new FormGroup({
    email: new FormControl(''),
    senha: new FormControl('')
  });
  erro: string = null;

  // Tradução dos erros mais comuns
  set _erro(e: string) {
    console.log(e);
    switch (e) {
      case 'The password is invalid or the user does not have a password.':
        this.erro = 'Senha inválida';
        break;
      case 'Too many unsuccessful login attempts. Please include reCaptcha verification or try again later.':
        this.erro = 'Muitas tentativas de login. Tente novamente mais tarde';
        break;
      case 'There is no user record corresponding to this identifier. The user may have been deleted.':
        this.erro = 'Usuário não cadastrado';
        break;
      default:
        this.erro = e;
        break;
    }
  }

  constructor(
    private authService: AuthService,
    private overlayService: OverlayService,
    private navController: NavController,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}

  async onSubmit() {
    const loading = await this.overlayService.loading();
    try {
      const credentials = await this.authService.login(this.loginForm.value);
      this.navController.navigateForward(
        this.route.snapshot.queryParamMap.get('redirect') || '/home'
      );
    } catch (e) {
      console.warn(e);
      this._erro = e.message;
    } finally {
      loading.dismiss();
    }
  }
}
