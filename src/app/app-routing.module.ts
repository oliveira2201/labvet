import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'requisicoes', pathMatch: 'full' },
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  // },
  {
    path: 'login',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'exames',
    loadChildren: './exames/exames.module#ExamesModule'
    // canLoad: [AuthGuard]
  },
  {
    path: 'requisicoes',
    loadChildren: './requisicoes/requisicoes.module#RequisicoesModule'
  },
  { path: 'grupos', loadChildren: './grupos/grupos.module#GruposModule' },
  {
    path: 'configuracoes',
    canActivateChild: [AuthGuard],
    loadChildren:
      './navegacao/pages/configuracoes-lista/configuracoes-lista.module#ConfiguracoesListaPageModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
