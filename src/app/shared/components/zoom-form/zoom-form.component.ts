import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-zoom-form',
  templateUrl: './zoom-form.component.html',
  styleUrls: ['./zoom-form.component.scss']
})
export class ZoomFormComponent implements OnInit {
  @Input() valor: string;
  @Input() titulo: string;
  constructor(private modalController: ModalController) {}

  async closeModal() {
    await this.modalController.dismiss(this.valor);
  }

  ngOnInit() {}
}
