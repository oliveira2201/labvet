# Details

Date : 2019-12-21 19:31:51

Directory c:\Users\olive\Documents\programas\labAdmin

Total : 100 files,  3105 codes, 307 comments, 455 blanks, all 3867 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [e2e\src\app.e2e-spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/e2e/src/app.e2e-spec.ts) | TypeScript | 11 | 0 | 4 | 15 |
| [e2e\src\app.po.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/e2e/src/app.po.ts) | TypeScript | 9 | 0 | 3 | 12 |
| [src\app\app-routing.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app-routing.module.ts) | TypeScript | 30 | 1 | 3 | 34 |
| [src\app\app.component.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app.component.html) | Django HTML | 23 | 0 | 5 | 28 |
| [src\app\app.component.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\app.component.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app.component.spec.ts) | TypeScript | 36 | 1 | 11 | 48 |
| [src\app\app.component.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app.component.ts) | TypeScript | 24 | 0 | 4 | 28 |
| [src\app\app.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/app.module.ts) | TypeScript | 31 | 0 | 6 | 37 |
| [src\app\auth\auth-routing.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/auth-routing.module.ts) | TypeScript | 10 | 0 | 3 | 13 |
| [src\app\auth\auth.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/auth.module.ts) | TypeScript | 7 | 0 | 3 | 10 |
| [src\app\auth\guards\auth.guard.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/guards/auth.guard.spec.ts) | TypeScript | 12 | 0 | 4 | 16 |
| [src\app\auth\guards\auth.guard.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/guards/auth.guard.ts) | TypeScript | 34 | 12 | 5 | 51 |
| [src\app\auth\pages\login\login.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/pages/login/login.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\auth\pages\login\login.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/pages/login/login.page.html) | Django HTML | 74 | 0 | 2 | 76 |
| [src\app\auth\pages\login\login.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/pages/login/login.page.scss) | SCSS | 0 | 7 | 2 | 9 |
| [src\app\auth\pages\login\login.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/pages/login/login.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\auth\pages\login\login.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/pages/login/login.page.ts) | TypeScript | 56 | 1 | 6 | 63 |
| [src\app\auth\services\auth.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/services/auth.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\auth\services\auth.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/auth/services/auth.service.ts) | TypeScript | 24 | 0 | 6 | 30 |
| [src\app\core\classes\repository.class.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/core/classes/repository.class.ts) | TypeScript | 35 | 0 | 9 | 44 |
| [src\app\core\services\overlay.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/core/services/overlay.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\core\services\overlay.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/core/services/overlay.service.ts) | TypeScript | 17 | 0 | 3 | 20 |
| [src\app\exames\components\exames-item\exames-item.component.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/components/exames-item/exames-item.component.html) | Django HTML | 4 | 0 | 3 | 7 |
| [src\app\exames\components\exames-item\exames-item.component.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/components/exames-item/exames-item.component.scss) | SCSS | 12 | 0 | 3 | 15 |
| [src\app\exames\components\exames-item\exames-item.component.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/components/exames-item/exames-item.component.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\exames\components\exames-item\exames-item.component.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/components/exames-item/exames-item.component.ts) | TypeScript | 12 | 0 | 2 | 14 |
| [src\app\exames\exames-routing.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/exames-routing.module.ts) | TypeScript | 30 | 6 | 3 | 39 |
| [src\app\exames\exames.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/exames.module.ts) | TypeScript | 7 | 0 | 3 | 10 |
| [src\app\exames\models\exames.model.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/models/exames.model.ts) | TypeScript | 29 | 0 | 1 | 30 |
| [src\app\exames\pages\exames-lista\exames-lista.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-lista/exames-lista.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\exames\pages\exames-lista\exames-lista.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-lista/exames-lista.page.html) | Django HTML | 23 | 0 | 3 | 26 |
| [src\app\exames\pages\exames-lista\exames-lista.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-lista/exames-lista.page.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\exames\pages\exames-lista\exames-lista.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-lista/exames-lista.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\exames\pages\exames-lista\exames-lista.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-lista/exames-lista.page.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\exames\pages\exames-salvar\exames-salvar.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-salvar/exames-salvar.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\exames\pages\exames-salvar\exames-salvar.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-salvar/exames-salvar.page.html) | Django HTML | 196 | 0 | 10 | 206 |
| [src\app\exames\pages\exames-salvar\exames-salvar.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-salvar/exames-salvar.page.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\exames\pages\exames-salvar\exames-salvar.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-salvar/exames-salvar.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\exames\pages\exames-salvar\exames-salvar.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/pages/exames-salvar/exames-salvar.page.ts) | TypeScript | 173 | 85 | 22 | 280 |
| [src\app\exames\services\exames.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/services/exames.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\exames\services\exames.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/exames/services/exames.service.ts) | TypeScript | 103 | 37 | 12 | 152 |
| [src\app\grupos\components\grupos-item\grupos-item.component.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/components/grupos-item/grupos-item.component.html) | Django HTML | 3 | 0 | 1 | 4 |
| [src\app\grupos\components\grupos-item\grupos-item.component.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/components/grupos-item/grupos-item.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\grupos\components\grupos-item\grupos-item.component.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/components/grupos-item/grupos-item.component.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\grupos\components\grupos-item\grupos-item.component.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/components/grupos-item/grupos-item.component.ts) | TypeScript | 12 | 0 | 2 | 14 |
| [src\app\grupos\grupos-routing.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/grupos-routing.module.ts) | TypeScript | 34 | 0 | 3 | 37 |
| [src\app\grupos\grupos.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/grupos.module.ts) | TypeScript | 11 | 0 | 4 | 15 |
| [src\app\grupos\models\grupos.model.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/models/grupos.model.ts) | TypeScript | 28 | 0 | 2 | 30 |
| [src\app\grupos\pages\grupos-lista\grupos-lista.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-lista/grupos-lista.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\grupos\pages\grupos-lista\grupos-lista.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-lista/grupos-lista.page.html) | Django HTML | 23 | 0 | 2 | 25 |
| [src\app\grupos\pages\grupos-lista\grupos-lista.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-lista/grupos-lista.page.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\grupos\pages\grupos-lista\grupos-lista.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-lista/grupos-lista.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\grupos\pages\grupos-lista\grupos-lista.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-lista/grupos-lista.page.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\grupos\pages\grupos-salvar\grupos-salvar.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-salvar/grupos-salvar.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\grupos\pages\grupos-salvar\grupos-salvar.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-salvar/grupos-salvar.page.html) | Django HTML | 38 | 0 | 3 | 41 |
| [src\app\grupos\pages\grupos-salvar\grupos-salvar.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-salvar/grupos-salvar.page.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\grupos\pages\grupos-salvar\grupos-salvar.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-salvar/grupos-salvar.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\grupos\pages\grupos-salvar\grupos-salvar.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/pages/grupos-salvar/grupos-salvar.page.ts) | TypeScript | 91 | 2 | 8 | 101 |
| [src\app\grupos\services\grupos.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/services/grupos.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\grupos\services\grupos.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/grupos/services/grupos.service.ts) | TypeScript | 51 | 19 | 8 | 78 |
| [src\app\home\home.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/home/home.module.ts) | TypeScript | 21 | 0 | 3 | 24 |
| [src\app\home\home.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/home/home.page.html) | Django HTML | 13 | 0 | 2 | 15 |
| [src\app\home\home.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/home/home.page.scss) | SCSS | 0 | 0 | 2 | 2 |
| [src\app\home\home.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/home/home.page.spec.ts) | TypeScript | 19 | 0 | 6 | 25 |
| [src\app\home\home.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/home/home.page.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\listas\models\listas.model.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/listas/models/listas.model.spec.ts) | TypeScript | 6 | 0 | 2 | 8 |
| [src\app\listas\models\listas.model.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/listas/models/listas.model.ts) | TypeScript | 4 | 0 | 1 | 5 |
| [src\app\listas\services\listas.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/listas/services/listas.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\listas\services\listas.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/listas/services/listas.service.ts) | TypeScript | 16 | 0 | 3 | 19 |
| [src\app\requisicoes\components\requisicoes-item\requisicoes-item.component.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/components/requisicoes-item/requisicoes-item.component.html) | Django HTML | 29 | 0 | 2 | 31 |
| [src\app\requisicoes\components\requisicoes-item\requisicoes-item.component.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/components/requisicoes-item/requisicoes-item.component.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\requisicoes\components\requisicoes-item\requisicoes-item.component.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/components/requisicoes-item/requisicoes-item.component.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\requisicoes\components\requisicoes-item\requisicoes-item.component.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/components/requisicoes-item/requisicoes-item.component.ts) | TypeScript | 13 | 0 | 4 | 17 |
| [src\app\requisicoes\models\requisicaomodel.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/models/requisicaomodel.ts) | TypeScript | 34 | 0 | 1 | 35 |
| [src\app\requisicoes\pages\requisicoes-lista\requisicoes-lista.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-lista/requisicoes-lista.module.ts) | TypeScript | 24 | 0 | 5 | 29 |
| [src\app\requisicoes\pages\requisicoes-lista\requisicoes-lista.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-lista/requisicoes-lista.page.html) | Django HTML | 23 | 0 | 2 | 25 |
| [src\app\requisicoes\pages\requisicoes-lista\requisicoes-lista.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-lista/requisicoes-lista.page.scss) | SCSS | 0 | 0 | 1 | 1 |
| [src\app\requisicoes\pages\requisicoes-lista\requisicoes-lista.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-lista/requisicoes-lista.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\requisicoes\pages\requisicoes-lista\requisicoes-lista.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-lista/requisicoes-lista.page.ts) | TypeScript | 23 | 1 | 4 | 28 |
| [src\app\requisicoes\pages\requisicoes-salvar\requisicoes-salvar.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-salvar/requisicoes-salvar.module.ts) | TypeScript | 23 | 0 | 5 | 28 |
| [src\app\requisicoes\pages\requisicoes-salvar\requisicoes-salvar.page.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-salvar/requisicoes-salvar.page.html) | Django HTML | 199 | 0 | 1 | 200 |
| [src\app\requisicoes\pages\requisicoes-salvar\requisicoes-salvar.page.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-salvar/requisicoes-salvar.page.scss) | SCSS | 19 | 7 | 6 | 32 |
| [src\app\requisicoes\pages\requisicoes-salvar\requisicoes-salvar.page.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-salvar/requisicoes-salvar.page.spec.ts) | TypeScript | 22 | 0 | 6 | 28 |
| [src\app\requisicoes\pages\requisicoes-salvar\requisicoes-salvar.page.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/pages/requisicoes-salvar/requisicoes-salvar.page.ts) | TypeScript | 326 | 22 | 25 | 373 |
| [src\app\requisicoes\requisicoes-routing.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/requisicoes-routing.module.ts) | TypeScript | 24 | 1 | 3 | 28 |
| [src\app\requisicoes\requisicoes.module.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/requisicoes.module.ts) | TypeScript | 11 | 0 | 4 | 15 |
| [src\app\requisicoes\services\relatorios.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/services/relatorios.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\requisicoes\services\relatorios.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/services/relatorios.service.ts) | TypeScript | 369 | 6 | 15 | 390 |
| [src\app\requisicoes\services\requisicoes.service.spec.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/services/requisicoes.service.spec.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\app\requisicoes\services\requisicoes.service.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/app/requisicoes/services/requisicoes.service.ts) | TypeScript | 16 | 0 | 3 | 19 |
| [src\assets\shapes.svg](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/assets/shapes.svg) | XML | 1 | 0 | 1 | 2 |
| [src\environments\environment.prod.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/environments/environment.prod.ts) | TypeScript | 3 | 0 | 1 | 4 |
| [src\environments\environment.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/environments/environment.ts) | TypeScript | 13 | 11 | 3 | 27 |
| [src\global.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/global.scss) | SCSS | 10 | 13 | 4 | 27 |
| [src\index.html](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/index.html) | Django HTML | 18 | 0 | 8 | 26 |
| [src\main.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/main.ts) | TypeScript | 9 | 0 | 4 | 13 |
| [src\polyfills.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/polyfills.ts) | TypeScript | 2 | 55 | 10 | 67 |
| [src\test.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/test.ts) | TypeScript | 13 | 4 | 4 | 21 |
| [src\theme\variables.scss](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/theme/variables.scss) | SCSS | 57 | 12 | 11 | 80 |
| [src\zone-flags.ts](file:///c%3A/Users/olive/Documents/programas/labAdmin/src/zone-flags.ts) | TypeScript | 1 | 4 | 1 | 6 |

[summary](results.md)