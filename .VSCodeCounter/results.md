# Summary

Date : 2019-12-21 19:31:51

Directory c:\Users\olive\Documents\programas\labAdmin

Total : 100 files,  3105 codes, 307 comments, 455 blanks, all 3867 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 72 | 2,340 | 268 | 374 | 2,982 |
| Django HTML | 13 | 666 | 0 | 44 | 710 |
| SCSS | 14 | 98 | 39 | 36 | 173 |
| XML | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 100 | 3,105 | 307 | 455 | 3,867 |
| e2e | 2 | 20 | 0 | 7 | 27 |
| e2e\src | 2 | 20 | 0 | 7 | 27 |
| src | 98 | 3,085 | 307 | 448 | 3,840 |
| src\app | 88 | 2,958 | 208 | 401 | 3,567 |
| src\app\auth | 11 | 271 | 20 | 46 | 337 |
| src\app\auth\guards | 2 | 46 | 12 | 9 | 67 |
| src\app\auth\pages | 5 | 175 | 8 | 21 | 204 |
| src\app\auth\pages\login | 5 | 175 | 8 | 21 | 204 |
| src\app\auth\services | 2 | 33 | 0 | 10 | 43 |
| src\app\core | 3 | 61 | 0 | 16 | 77 |
| src\app\core\classes | 1 | 35 | 0 | 9 | 44 |
| src\app\core\services | 2 | 26 | 0 | 7 | 33 |
| src\app\exames | 19 | 733 | 128 | 101 | 962 |
| src\app\exames\components | 4 | 50 | 0 | 14 | 64 |
| src\app\exames\components\exames-item | 4 | 50 | 0 | 14 | 64 |
| src\app\exames\models | 1 | 29 | 0 | 1 | 30 |
| src\app\exames\pages | 10 | 505 | 85 | 64 | 654 |
| src\app\exames\pages\exames-lista | 5 | 91 | 0 | 20 | 111 |
| src\app\exames\pages\exames-salvar | 5 | 414 | 85 | 44 | 543 |
| src\app\exames\services | 2 | 112 | 37 | 16 | 165 |
| src\app\grupos | 19 | 435 | 21 | 73 | 529 |
| src\app\grupos\components | 4 | 37 | 0 | 10 | 47 |
| src\app\grupos\components\grupos-item | 4 | 37 | 0 | 10 | 47 |
| src\app\grupos\models | 1 | 28 | 0 | 2 | 30 |
| src\app\grupos\pages | 10 | 265 | 2 | 42 | 309 |
| src\app\grupos\pages\grupos-lista | 5 | 91 | 0 | 19 | 110 |
| src\app\grupos\pages\grupos-salvar | 5 | 174 | 2 | 23 | 199 |
| src\app\grupos\services | 2 | 60 | 19 | 12 | 91 |
| src\app\home | 5 | 62 | 0 | 17 | 79 |
| src\app\listas | 4 | 35 | 0 | 10 | 45 |
| src\app\listas\models | 2 | 10 | 0 | 3 | 13 |
| src\app\listas\services | 2 | 25 | 0 | 7 | 32 |
| src\app\requisicoes | 21 | 1,217 | 37 | 108 | 1,362 |
| src\app\requisicoes\components | 4 | 64 | 0 | 13 | 77 |
| src\app\requisicoes\components\requisicoes-item | 4 | 64 | 0 | 13 | 77 |
| src\app\requisicoes\models | 1 | 34 | 0 | 1 | 35 |
| src\app\requisicoes\pages | 10 | 681 | 30 | 61 | 772 |
| src\app\requisicoes\pages\requisicoes-lista | 5 | 92 | 1 | 18 | 111 |
| src\app\requisicoes\pages\requisicoes-salvar | 5 | 589 | 29 | 43 | 661 |
| src\app\requisicoes\services | 4 | 403 | 6 | 26 | 435 |
| src\assets | 1 | 1 | 0 | 1 | 2 |
| src\environments | 2 | 16 | 11 | 4 | 31 |
| src\theme | 1 | 57 | 12 | 11 | 80 |

[details](details.md)