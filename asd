// playground requires you to assign document definition to a variable called dd

var dd = {
	content: [
		{
			image: 'sampleImage.jpg',
			width: 150,
			height: 100,
			            style: 'center'

		},
		
		{
    		layout: 'noBorders',
            table: {
            widths: [ '*', '*'],
            body: [
              [ {text: 'Nome:', bold: true}, {text: 'Requisição:', bold: true}],
              [ {text: 'Tutor:', bold: true}, {text: 'Sexo:', bold: true}],
              [ {text: 'Espécie:', bold: true}, {text: 'Idade:', bold: true}],
              [ {text: 'Veterinário:', bold: true}, {text: 'Raça:', bold: true}],
              [ {text: 'Entrada:', bold: true}, {text: 'Clínica:', bold: true}],
              [ {text: 'Saída:', bold: true}, ''],
            ],
            }
		},
		
		{
    		layout: 'noBorders',
            table: {
            widths: [ '*', '*'],
            body: [
              [ {text: 'Nome 1', fillColor: '#E12C88'}, {text: 'Requisição', bold: true}],
              [ {text: 'Tutor', bold: true}, {text: 'Sexo', bold: true}],
              [ {text: 'Espécie', bold: true}, {text: 'Idade', bold: true}],
              [ {text: 'Veterinário', bold: true}, {text: 'Raça', bold: true}],
              [ {text: 'Entrada', bold: true}, {text: 'Clínica', bold: true}],
              [ {text: 'Saída', bold: true}, ''],
            ],
        }
        
        
    }
		],
		
		
		styles:{
        center:
        {   
            alignment: 'center'
        }

    }
	
}